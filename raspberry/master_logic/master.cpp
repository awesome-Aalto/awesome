#include <iostream>
#include <chrono>
#include "master.h"

master::master()
    : m_running(false),
    m_mqttClient("#"),
    m_systemMsgHandler(&m_mqttClient),
    m_userMsgHandler(&m_mqttClient),
    debug_storageColor(unknown),
    m_systemRunning(false)
{

}

master::~master()
{
    m_running = false;
    m_userThread.detach();

    for (auto x : m_receivers)
        delete x;
    for (auto x : m_sorters)
        delete x;
    for (auto x : m_storages)
        delete x;
}

using time_point = std::chrono::system_clock::time_point;
using system_clock = std::chrono::system_clock;
using milliseconds = std::chrono::milliseconds;

void master::run(const bool& signalFlag, bool user_commands)
{
    //m_msgHandler.connect();
    m_running = true;

    if (user_commands)
    {
        m_userThread = std::thread(&master::askUserInput, std::ref(*this));
    }

    time_point lastrun = system_clock::now();

    while (signalFlag && m_running)
    {
        m_userCmdLock.lock();
        if (!m_userCmds.empty())
        {
            userCommand uc = m_userCmds.front();
            m_userCmds.pop();
            m_userCmdLock.unlock();
            if (uc == CMD_QUIT)
            {
                m_running = false;
            }
            //start message placeholder
            if(uc == CMD_START)
            {
                startSystem();
            }
            //stop message placeholder
            if(uc == CMD_STOP)
            {
                stopSystem();
            }
            //debugger for receiver wait state
            if(uc == CMD_WAIT_RECEIVER_RUN)
            {
                std::cout << "Receiver wait" << std::endl;
                receiver* rcvr = getReceiver();
                if(rcvr != nullptr)
                {
                    moduleWaitRunning(rcvr);
                }
            }
            if(uc == CMD_STOP_RECEIVER_WAIT)
            {
                std::cout << "Receiver end wait" << std::endl;
                receiver* rcvr = getReceiver();
                if(rcvr != nullptr)
                {
                    startModule(rcvr);
                }
            }
        }
        m_userCmdLock.unlock();

        if (m_systemMsgHandler.hasNewMessages())
        {
            handleMessages();
            receiver* rcvr = getReceiver(false);
            std::cout << std::endl;
            if(rcvr != nullptr)
            {
                std::cout << rcvr->getName() << " state: " << rcvr->getState() << std::endl;
            }
            else
            {
                std::cout << "No receivers connected" << std::endl;
            }
            sorter* srtr = getSorter(false);
            if(srtr != nullptr)
            {
                std::cout << srtr->getName() << "   state: " << srtr->getState() << " bricks: " << srtr->getBricks() << " storages: " << srtr->getStorages() << std::endl;
            }
            else
            {
                std::cout << "No sorters connected" << std::endl;
            }
            for(auto storage : m_storages)
            {
                std::cout << storage->getName() << " state: " << storage->getState() << " color: " << storage->getColor() << " stored: " << storage->getStored() << "/" << storage->getCapacity() << std::endl;
            }
            if(m_storages.size() <= 0)
            {
                std::cout << "No storages connected" << std::endl;
            }
            std::cout << std::endl;
        }
        if (m_userMsgHandler.hasNewMessages())
        {
            handleUserMessages();
        }
        //
        time_point now = system_clock::now();
        if(std::chrono::duration_cast<milliseconds>(now - lastrun).count() > 1000)
        {
            updateStatusToUI();
            lastrun = system_clock::now();
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

sender* master::parseSender(std::string sndr)
{
    moduleType type;

    if(sndr.find("receiver") != std::string::npos)
    {
        type = receiverType;
    }
    if(sndr.find("sorter") != std::string::npos)
    {
        type = sorterType;
    }
    if(sndr.find("storage") != std::string::npos)
    {
        type = storageType;
    }

    sender* s = new sender(type, sndr);
    return s;
}

void master::handleMessages()
{
    systemMessage msg = m_systemMsgHandler.getMessage();
    sender* sndr = parseSender(msg.sender);
    switch(msg.msg->getType())
    {
        case e_ok:
            ok(dynamic_cast<msg_ok*>(msg.msg), sndr);
            break;
        case e_connected:
            connected(dynamic_cast<msg_connected*>(msg.msg), sndr);
            break;
        case e_error:
            error(dynamic_cast<msg_error*>(msg.msg), sndr);
            break;
        case e_colorQuery:
            colorQuery(dynamic_cast<msg_colorQuery*>(msg.msg), sndr);
            break;
        case e_brickAccepted:
            brickAccepted(dynamic_cast<msg_brickAccepted*>(msg.msg), sndr);
            break;
        case e_storageQuery:
            storageQuery(dynamic_cast<msg_storageQuery*>(msg.msg), sndr);
            break;
        case e_brickPushed:
            brickPushed(dynamic_cast<msg_brickPushed*>(msg.msg), sndr);
            break;
        case e_ready:
            ready(dynamic_cast<msg_ready*>(msg.msg), sndr);
            break;
        case e_brickStored:
            brickStored(dynamic_cast<msg_brickStored*>(msg.msg), sndr);
            break;
        case e_finalPosition:
            finalPosition(dynamic_cast<msg_finalPosition*>(msg.msg), sndr);
            break;
        case e_disconnected:
            disconnected(dynamic_cast<msg_disconnected*>(msg.msg), sndr);
            break;
        default:
            break;
    }
    delete sndr;
    delete msg.msg;
}

void master::disconnected(msg_disconnected* msg, sender* sndr)
{
    (void)msg;
    switch(sndr->type)
    {
        case receiverType:
        {
            receiver* rcvr = getReceiver();
            if(rcvr == nullptr) return;
            rcvr->setState(ms_unknown);
            m_userMsgHandler.sendReceiverStatus(rcvr);
            for(auto receiver : m_receivers)
            {
                delete receiver;
            }
            while(m_receivers.size() > 0)
            {
                m_receivers.erase(m_receivers.begin());
            }
            std::cout << "receiver disconnected" << std::endl;
            break;
        }
        case sorterType:
        {
            sorter* srtr = getSorter();
            if(srtr == nullptr) return;
            srtr->setState(ms_unknown);
            m_userMsgHandler.sendSorterStatus(srtr);
            for(auto sorter : m_sorters)
            {
                delete sorter;
            }
            while(m_sorters.size() > 0)
            {
                m_sorters.erase(m_sorters.begin());
            }
            std::cout << "sorter disconnected" << std::endl;
            break;
        }
        case storageType:
        {
            storage* strg = getStorage(sndr->name);
            if(strg == nullptr) return;
            strg->setState(ms_unknown);
            m_userMsgHandler.sendStorageStatus(strg);
            removeStorage(sndr->name);
            std::cout << "storage " << sndr->name << " disconnected" << std::endl;
            break;
        }
        default:
            break;
    }
}

void master::ok(msg_ok* msg, sender* sndr)
{
    (void)msg;
    switch(sndr->type)
    {
        case receiverType:
            receiverOk(sndr);
            break;
        case sorterType:
            sorterOk(sndr);
            break;
        case storageType:
            storageOk(sndr);
            break;
        default:
            break;
    }
}

void master::receiverOk(sender* sndr)
{
    (void)sndr;
    receiver* rcvr = getReceiver();
    if(rcvr == nullptr) return;
    switch(rcvr->getState())
    {
        case ms_starting:
            rcvr->setState(ms_started);
            break;
        case ms_stopping:
            rcvr->setState(ms_stopped);
            break;
        default:
            break;
    }
}

void master::sorterOk(sender* sndr)
{
    (void)sndr;
    sorter* srtr = getSorter();
    if(srtr == nullptr) return;
    switch(srtr->getState())
    {
        case ms_starting:
            srtr->setState(ms_started);
            break;
        case ms_stopping:
            srtr->setState(ms_stopped);
            break;
        default:
            break;
    }
}

void master::storageOk(sender* sndr)
{
    storage* strg = getStorage(sndr->name);
    if(strg == nullptr) return;
    switch(strg->getState())
    {
        case ms_initializing:
            strg->setState(ms_initialized);
            break;
        case ms_starting:
            strg->setState(ms_started);
            break;
        case ms_stopping:
            strg->setState(ms_stopped);
            break;
        default:
            break;
    }
}

void master::connected(msg_connected* msg, sender* sndr)
{
    switch(sndr->type)
    {
        case receiverType:
        {
            receiver* r = new receiver(sndr->name);
            m_receivers.push_back(r);
            sendOk(sndr);
            r->setState(ms_initializing);
            m_systemMsgHandler.init();
            std::cout << "Receiver connected" << std::endl;
            break;
        }
        case sorterType:
        {
            sorter* srtr = new sorter(sndr->name, 3, (int)(msg->getParameter()));
            m_sorters.push_back(srtr);
            sendOk(sndr);
            m_systemMsgHandler.init();
            srtr->setState(ms_initializing);
            std::cout << "Sorter connected with " << (int)(msg->getParameter()) << " storages " << std::endl;
            break;
        }
        case storageType:
        {
            storage* existing = getStorage((int)(msg->getParameter()));
            if(existing == nullptr)
            {
                storage* strg = new storage(sndr->name, 3, (int)(msg->getParameter()));
                strg->setColor(debug_getStorageColor());
                m_storages.push_back(strg);
                sendOk(sndr);
                m_systemMsgHandler.init();
                strg->setState(ms_initializing);
                std::cout << "Storage " << (int)(msg->getParameter()) << " connected" << std::endl;
            }
            else
            {
                std::cout << "Storage " << (int)(msg->getParameter()) << " already exists" << std::endl;
            }
            break;
        }
        default:
            break;
    }
}

void master::error(msg_error* msg, sender* sndr)
{
    (void)msg;
    std::cout << "error message received!" << std::endl;
    std::cout << std::endl;
    //error handling placeholder
    m_systemMsgHandler.setReceiver(sndr->name);
    m_systemMsgHandler.init();
}

void master::colorQuery(msg_colorQuery* msg, sender* sndr)
{
    m_systemMsgHandler.setReceiver(sndr->name);
    if(roomForColor(msg->getColor()))
    {
        sorter* srtr = getSorter();
        if(srtr != nullptr)
        {
            if(srtr->getBricks() == 0)
            {
                if(!srtr->isRunning())
                {
                    startModule(srtr);
                }
                m_systemMsgHandler.setReceiver(sndr->name);
                m_systemMsgHandler.queryOk(true);
            }
            else
            {
                receiver* rcvr = getReceiver();
                if(rcvr != nullptr)
                {
                    rcvr->setState(ms_waitingHandling);
                    rcvr->setWaitingColor(msg->getColor());
                    m_systemMsgHandler.wait();
                    std::cout << std::endl;
                    std::cout << "Brick ok, but sorter busy." << std::endl;
                    std::cout << std::endl;
                }
            }
            return;
        }
    }
    std::cout << std::endl;
    std::cout << "Ignoring brick, color: " << msg->getColor() << std::endl;
    std::cout << "Storage status:" << std::endl;
    for(auto storage : m_storages)
    {
        std::cout << storage->getName() << ". Full: " << storage->isFull() << " Color: " << storage->getColor() << " Stored: " << storage->getStored() << "/" << storage->getCapacity() << std::endl;
    }
    std::cout << std::endl;

    m_systemMsgHandler.queryOk(false);
}

void master::brickAccepted(msg_brickAccepted* msg, sender* sndr)
{
    if(msg->isAccepted())
    {
        std::cout << "Brick accepted" << std::endl;
        sorter* srtr = getSorter();
        if(srtr == nullptr) return;
        srtr->addBrick();
        srtr->setBrickColor(msg->getColor());
        std::cout << "Brick added to sorter" << std::endl;
        std::cout << std::endl;
        //fallback error handling
        if(!srtr->isRunning())
        {
            startModule(srtr);
        }
    }
    sendOk(sndr);
}

void master::storageQuery(msg_storageQuery* msg, sender* sndr)
{
    std::cout << "storagequery from " << sndr->name << std::endl;
    storage* strg = getStorage(msg->getStorageNum());
    if(strg != nullptr && !strg->isFull() && msg->getColor() == strg->getColor())
    {
        if(!strg->isBusy())
        {
            startModule(strg);
            m_systemMsgHandler.setReceiver(sndr->name);
            m_systemMsgHandler.queryOk(true);
        }
        else
        {
            m_systemMsgHandler.setReceiver(sndr->name);
            m_systemMsgHandler.wait();
            sorter* srtr = getSorter();
            if(srtr == nullptr) return;
            srtr->setState(ms_waitingHandling);
            srtr->setWaitingStorage(msg->getStorageNum());
            std::cout << std::endl;
            std::cout << "Brick ok, but storage busy." << std::endl;
            std::cout << std::endl;
        }
    }
    else
    {
        m_systemMsgHandler.setReceiver(sndr->name);
        m_systemMsgHandler.queryOk(false);
        std::cout << std::endl;
        std::cout << "Brick ignored, color: " << msg->getColor() << " reason: " << std::endl;
        if(strg == nullptr)
        {
            std::cout << "there is no storage" << std::endl;
        }
        else
        {
            if(strg->isFull())
            {
                std::cout << "storage is full" << std::endl;
            }
            if(msg->getColor() != strg->getColor())
            {
                std::cout << "wrong color. storage color: " << strg->getColor() << std::endl;
            }
        }
        std::cout << std::endl;
        //If the storage is the last one in the conveyor,
        //and the brick is not accepted, the brick is ignored and removed
        sorter* srtr = getSorter();
        if(strg != nullptr && srtr != nullptr)
        {
            std::cout << "Checking if end of sorter. Storage: " << strg->getNumber() << " No of Storages: " << srtr->getStorages() << std::endl;
            if(strg->getNumber() >= srtr->getStorages())
            {
                srtr->removeBrick();
                std::cout << "Brick removed from sorter. Sorter bricks: " << srtr->getBricks() << std::endl;
                receiverEndWait();
            }
            std::cout << std::endl;
        }
    }
}

void master::brickPushed(msg_brickPushed* msg, sender* sndr)
{
    sorter* srtr = getSorter();
    bool receiverStarting = false;
    if(srtr == nullptr) return;

    if(msg->isPushed())
    {
        std::cout << std::endl;
        std::cout << "removing brick from sorter" << std::endl;
        std::cout << std::endl;
        srtr->removeBrick();
    }

    sendOk(sndr);

    receiver* rcvr = getReceiver();
    if(rcvr != nullptr && (msg->isPushed() || msg->getStorageNum() == 3))
    {
        receiverStarting = receiverEndWait();
    }

    if(srtr->getBricks() > 0)
    {
        startModule(srtr);
        std::cout << "sorter started" << std::endl;
    }
    else if(!receiverStarting)
    {
        stopModule(srtr);
        std::cout << "sorter stopped. bricks: " << srtr->getBricks() << " receiver starting: " << receiverStarting << std::endl;
    }
    std::cout << std::endl;
}

void master::ready(msg_ready* msg, sender* sndr)
{
    (void)msg;
    switch(sndr->type)
    {
        case receiverType:
        {
            receiver* rcvr = getReceiver();
            if(rcvr == nullptr) return;
            rcvr->setState(ms_initialized);
            if(m_systemRunning)
            {
                startModule(rcvr);
            }
            break;
        }
        case sorterType:
        {
            sorter* srtr = getSorter();
            if(srtr == nullptr) return;
            srtr->setState(ms_initialized);
            if(m_systemRunning)
            {
                stopModule(srtr);
            }
            break;
        }
        case storageType:
        {
            storage* strg = getStorage(sndr->name);
            if(strg == nullptr) return;
            strg->setState(ms_initialized);
            if(m_systemRunning)
            {
                stopModule(strg);
            }
            break;
        }
        default:
            break;
    }
    sendOk(sndr);
}

void master::brickStored(msg_brickStored* msg, sender* sndr)
{
    (void)msg;
    storage* strg = getStorage(sndr->name);
    if(strg == nullptr) return;

    sendOk(sndr);
    strg->setBrick();
    stopModule(strg);
    if(strg->isFull())
    {
        strg->setState(ms_full);
        m_userMsgHandler.sendStorageFull(strg);
        std::cout << "storage full" << std::endl;
        std::cout << std::endl;
    }

    //if sorter is waiting for push, notify
    sorterEndWait();
}

void master::finalPosition(msg_finalPosition* msg, sender* sndr)
{
    (void)msg;
    storage* strg = getStorage(sndr->name);
    if(strg == nullptr) return;

    strg->setState(ms_full);
    sendOk(sndr);
}

void master::handleUserMessages()
{
    userMessage msg = m_userMsgHandler.getMessage();
    switch(msg.type)
    {
        case umsg_receiverStatus:
            break;
        case umsg_sorterStatus:
            break;
        case umsg_storageStatus:
            break;
        case umsg_startSystem:
            startSystem();
            break;
        case umsg_stopSystem:
            stopSystem();
            break;
        case umsg_setStorageColors:
            setStorageColors(msg);
            break;
        case umsg_storageFull:
            break;
        case umsg_storageEmpty:
            emptyStorage(msg);
            break;
        case umsg_sorterEmpty:
            emptySorter();
        case umsg_receiverEndWait:
            receiverEndWait();
        case umsg_sorterEndWait:
            sorterEndWait();
        default:
            break;
    }
}

void master::startSystem()
{
    m_systemRunning = true;
    std::cout << "Starting the system" << std::endl;
    receiver* rcvr = getReceiver();
    sorter* srtr = getSorter();

    if(rcvr != nullptr)
    {
        startModule(rcvr);
    }
    if(srtr != nullptr)
    {
        stopModule(srtr);
    }
    for(auto strg : m_storages)
    {
        stopModule(strg);
    }
}

void master::stopSystem()
{
    m_systemRunning = false;
    std::cout << "Stopping the system" << std::endl;
    receiver* rcvr = getReceiver();
    sorter* srtr = getSorter();

    if(rcvr != nullptr)
    {
        stopModule(rcvr);
    }
    if(srtr != nullptr)
    {
        stopModule(srtr);
    }
    for(auto strg : m_storages)
    {
        stopModule(strg);
    }
}

int master::parseIntFromMsg(userMessage msg, std::string field)
{
    std::string strNumber = (msg.data[field]).dump();
    std::string s = strNumber.substr(1, strNumber.length());
    return std::stoi(s);
}

void master::emptyStorage(userMessage msg)
{
    int number = parseIntFromMsg(msg, "number");
    std::cout << "emptying storage: " << number << std::endl;
    storage* strg = getStorage(number);
    if(strg != nullptr)
    {
        strg->empty();
        strg->setState(ms_initializing);
        m_systemMsgHandler.setReceiver(strg->getName());
        m_systemMsgHandler.init();
    }
}

void master::emptySorter()
{
    std::cout << "emptying sorter" << std::endl;
    sorter* srtr = getSorter();
    if(srtr != nullptr)
    {
        srtr->setBricks(0);
    }
}

void master::setStorageColors(userMessage msg)
{
    int number = parseIntFromMsg(msg, "number");
    int intc = parseIntFromMsg(msg, "color");
    color c = (color)intc;
    std::cout << "set storage: " << number << " color to: " << intc << std::endl;
    storage* strg = getStorage(number);
    if(strg != nullptr)
    {
        strg->setColor(c);
    }
}

bool master::receiverEndWait()
{
    bool found = false;
    receiver* rcvr = getReceiver();
    if(rcvr == nullptr) return false;

    if(rcvr->getState() == ms_waitingHandling)
    {
        found = roomForColor(rcvr->getWaitingColor());
        m_systemMsgHandler.setReceiver(rcvr->getName());
        m_systemMsgHandler.queryOk(found);

        if(found)
        {
            std::cout << "Staring waiting receiver" << std::endl;
            rcvr->setState(ms_started);
        }
    }

    sorter* srtr = getSorter();
    if(srtr == nullptr) return false;

    if(found)
    {
        startModule(srtr);
        std::cout << "sorter started" << std::endl;
    }

    return found;
}

void master::sorterEndWait()
{
    sorter* srtr = getSorter();
    if(srtr == nullptr) return;
    storage* strg = getStorage(srtr->getWaitingStorage());
    if(strg == nullptr) return;
    if(srtr->getState() == ms_waitingHandling)
    {
        if(!strg->isFull())
        {
            startModule(strg);
        }
        m_systemMsgHandler.setReceiver(srtr->getName());
        m_systemMsgHandler.queryOk(!strg->isFull());
        srtr->setState(ms_started);
        std::cout << "sorter was waiting, commanding to push" << std::endl;
        std::cout << std::endl;
    }
}

void master::sendOk(sender* sndr)
{
    m_systemMsgHandler.setReceiver(sndr->name);
    m_systemMsgHandler.ok();
}

void master::stopModule(module* m)
{
    m->setState(ms_stopping);
    m_systemMsgHandler.setReceiver(m->getName());
    m_systemMsgHandler.stopConveyor();
}

void master::startModule(module* m)
{
    m->setState(ms_starting);
    m_systemMsgHandler.setReceiver(m->getName());
    m_systemMsgHandler.startConveyor();
}

void master::moduleWaitRunning(module* m)
{
    waitModule(m, ms_waitingRunning);
}

void master::moduleWaitHandling(module* m)
{
    waitModule(m, ms_waitingHandling);
}

void master::waitModule(module* m, moduleState waitState)
{
    m->setState(waitState);
    m_systemMsgHandler.setReceiver(m->getName());
    m_systemMsgHandler.wait();
}

storage* master::getStorage(int number)
{
    for(auto storage : m_storages)
    {
        if(storage->getNumber() == number)
        {
            return storage;
        }
    }
    std::cout << "null storage" << std::endl;
    return nullptr;
}

storage* master::getStorage(unsigned char num)
{
    return getStorage((int)num);
}

storage* master::getStorage(std::string name)
{
    for(auto storage : m_storages)
    {
        if(!storage->getName().compare(name))
        {
            return storage;
        }
    }
    std::cout << "null storage" << std::endl;
    //m_systemMsgHandler.setReceiver(name);
    //m_systemMsgHandler.error();
    return nullptr;
}

void master::removeStorage(std::string name)
{
    int idx = 0;
    for(auto storage : m_storages)
    {
        if(!storage->getName().compare(name))
        {
            delete storage;
            break;
        }
        idx++;
    }
    m_storages.erase(m_storages.begin()+idx);
}

receiver* master::getReceiver(bool dgb)
{
    if(m_receivers.size() > 0)
    {
        return m_receivers[0];
    }
    else
    {
        if(dgb)
        {
            std::cout << "null receiver" << std::endl;
        }
        return nullptr;
    }
}

sorter* master::getSorter(bool dgb)
{
    if(m_sorters.size() > 0)
    {
        return m_sorters[0];
    }
    else
    {
        if(dgb)
        {
            std::cout << "null sorter" << std::endl;
        }
        return nullptr;
    }
}

bool master::roomForColor(color c)
{
    int places = 0;
    for(auto storage : m_storages)
    {
        if(!storage->isFull() && storage->getColor() == c)
        {
            places += storage->getCapacity() - storage->getStored();
        }
    }
    sorter* srtr = getSorter();
    if(srtr != nullptr)
    {
        if(srtr->getBrickColor() == c && srtr->getBricks() > 0)
        {
            places--;
        }
    }
    return places > 0;
}

void master::askUserInput()
{
    using namespace std;

    while (m_running)
    {
        userCommand ans = CMD_UNKNOWN;

        cout << "Choose what to do." << endl;
        cout << "'s' to start" << endl;
        cout << "'q' to quit" << endl;
        cout << "'t' to stop" << endl;
        cout << "'w' to receiver run wait" << endl;
        cout << "'e' to end receiver run wait" << endl;
        cout << "> ";

        char x;
        cin >> x;

        cout << "user input: '" << x << "'" << endl;

        switch(x)
        {
            case 's':
                ans = CMD_START;
                break;
            case 'q':
                ans = CMD_QUIT;
                break;
            case 't':
                ans = CMD_STOP;
                break;
            case 'w':
                ans = CMD_WAIT_RECEIVER_RUN;
                break;
            case 'e':
                ans = CMD_STOP_RECEIVER_WAIT;
                break;
            default:
                ans = CMD_UNKNOWN;
                break;
        }
        m_userCmdLock.lock();
        m_userCmds.push(ans);
        m_userCmdLock.unlock();

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

color master::debug_getStorageColor()
{
    color newColor;
    if(debug_storageColor == unknown || debug_storageColor == magenta)
    {
        newColor = red;
    }
    if(debug_storageColor == red)
    {
        newColor = green;
    }
    if(debug_storageColor == green)
    {
        newColor = blue;
    }
    if(debug_storageColor == blue)
    {
        newColor = yellow;
    }
    if(debug_storageColor == yellow)
    {
        newColor = magenta;
    }
    debug_storageColor = newColor;
    return newColor;
}

void master::updateStatusToUI()
{
    receiver* rcvr = getReceiver(false);
    if(rcvr != nullptr)
    {
        updateReceiverAcceptedColors(rcvr);
        m_userMsgHandler.sendReceiverStatus(rcvr);
    }
    sorter* srtr = getSorter(false);
    if(srtr != nullptr)
    {
        m_userMsgHandler.sendSorterStatus(srtr);
    }
    for(auto storage : m_storages)
    {
        m_userMsgHandler.sendStorageStatus(storage);
    }
    m_userMsgHandler.sendSystemStatus(m_systemRunning);
}

void master::updateReceiverAcceptedColors(receiver* rcvr)
{
    std::vector<color> newColors;
    for(auto storage : m_storages)
    {
        if(!storage->isFull())
        {
            newColors.push_back(storage->getColor());
        }
    }
    rcvr->setValidColors(newColors);
}
