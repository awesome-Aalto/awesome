#ifndef PG26_MESSAGEHANDLERBASE_H
#define PG26_MESSAGEHANDLERBASE_H

#include <mutex>
#include <queue>
#include <string>

// forward declaration
class mqttClientRaspberry;


template <typename T>
class messageHandlerBase
{
public:
    messageHandlerBase(mqttClientRaspberry* mqttClient)
        : m_mqttClient(mqttClient)
    {}

    virtual ~messageHandlerBase()
    {}

protected:

    std::queue<T> m_msgQueue;
    std::mutex m_queueLock;
    
    std::string m_sendTopic;
    mqttClientRaspberry* m_mqttClient;
};


#endif
