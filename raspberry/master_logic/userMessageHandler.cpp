#include "json.hpp"
#include "userMessageHandler.h"

using json = nlohmann::json;

userMessageHandler::userMessageHandler(mqttClientRaspberry* mqttClient) : messageHandlerBase(mqttClient)
{
    m_mqttClient->setUserCallbackFn(std::bind(&userMessageHandler::msgParser, this, std::placeholders::_1));
    m_mqttClient->startReceiving();
    m_sendTopic = "listen/ui";
}


userMessageHandler::~userMessageHandler()
{}


bool userMessageHandler::hasNewMessages()
{
    m_queueLock.lock();
    bool b = !m_msgQueue.empty();
    m_queueLock.unlock();
    return b;
}


userMessage userMessageHandler::getMessage()
{
    m_queueLock.lock();
    userMessage msg = m_msgQueue.front();
    m_msgQueue.pop();
    m_queueLock.unlock();
    return msg;
}

void userMessageHandler::msgParser(std::string msg)
{
    auto container = json::parse(msg);
    std::string strType = (container["type"]).dump();
    userMessageType type = parseUserMessageType(strType);
    std::string strMsg = (container["message"]).dump();
    json message = json::parse(strMsg);
    userMessage usrMsg = {type, message};
    m_queueLock.lock();
    m_msgQueue.push(usrMsg);
    m_queueLock.unlock();
}

void userMessageHandler::sendMessage(std::string msg)
{
    //std::cout << "sending: " << msg << std::endl;
    m_mqttClient->publish(NULL, m_sendTopic.c_str(), msg.length(), (const unsigned char*)msg.c_str());
}

void userMessageHandler::sendReceiverStatus(receiver* r)
{
    //std::cout << "send receiver status" << std::endl;
    json j;
    j["type"] = umsg_receiverStatus;
    json message;
    message["state"] = r->getState();
    message["name"] = r->getName();
    message["colors"] = json(r->getValidColors());
    j["message"] = message.dump();
    sendMessage(j.dump());
}

void userMessageHandler::sendSorterStatus(sorter* s)
{
    //std::cout << "send sorter status" << std::endl;
    json j;
    j["type"] = umsg_sorterStatus;
    json message;
    message["state"] = s->getState();
    message["name"] = s->getName();
    message["storages"] = s->getStorages();
    message["bricks"] = s->getBricks();
    j["message"] = message.dump();
    sendMessage(j.dump());
}

void userMessageHandler::sendStorageStatus(storage* s)
{
    //std::cout << "send storage status" << std::endl;
    json j;
    j["type"] = umsg_storageStatus;
    json message;
    message["state"] = s->getState();
    message["name"] = s->getName();
    message["color"] = s->getColor();
    message["number"] = s->getNumber();
    message["full"] = s->isFull();
    message["capacity"] = s->getCapacity();
    message["stored"] = s->getStored();
    j["message"] = message.dump();
    sendMessage(j.dump());
}

void userMessageHandler::sendSystemStatus(bool running)
{
    json j;
    j["type"] = umsg_systemStatus;
    json message;
    message["running"] = running;
    j["message"] = message.dump();
    sendMessage(j.dump());
}

void userMessageHandler::sendStorageFull(storage* s)
{
    json j;
    j["type"] = umsg_storageFull;
    json message;
    message["number"] = s->getNumber();
    j["message"] = message.dump();
    sendMessage(j.dump());
}

userMessageType userMessageHandler::parseUserMessageType(std::string str)
{
    std::string s = str.substr(1, str.length());
    int intType = std::stoi(s);
    return (userMessageType)intType;
}
