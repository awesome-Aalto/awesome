#ifndef PG26_SYSTEMMESSAGEHANDLER_H
#define PG26_SYSTEMMESSAGEHANDLER_H

#include <string>
#include <iostream>
#include "message.h"
#include "systemMsgWrapper.h"
#include "messageHandlerBase.h"

struct systemMessage
{
    const std::string sender;
    message* msg;
};

struct outboundMessage
{
    uint8_t buffer[5];
    std::string topic;
    std::chrono::system_clock::time_point sendTime;
    int timesSent;
};


class systemMessageHandler : public messageHandlerBase<systemMessage>, public systemMsgWrapper
{
public:
    systemMessageHandler(mqttClientRaspberry* mqttClient);

    ~systemMessageHandler();

    bool hasNewMessages();

    systemMessage getMessage();

    void setReceiver(const std::string& name);

private:
    void sendMessage(messageType type, bool answerRequired, unsigned char param1, unsigned char param2);
    void msgParser(const std::string name, uint8_t* buf, uint32_t len);
    unsigned char getMessageId();
    void handleOutboundMessages();
    void purgeSendQueue(message* msg);
    void printBuffer(uint8_t* buf);
    void printOutboundMsg(outboundMessage msg);
    void printInboundMsg(std::string name, uint8_t* buf);
    void printMsgType(int msgType) const;

    unsigned char m_messageId;
    std::vector<outboundMessage> m_msgSendQueue;
    std::mutex m_sendQueueLock;
    std::atomic<bool> m_sending;
    std::thread m_senderThread;
};



#endif
