#include "modules.h"



module::module(moduleType m, const std::string& name)
    : m_type(m),
    m_running(false),
    m_name(name),
    m_state(ms_connected)
{}

void module::setState(moduleState newState)
{
    m_state = (int)newState;
}

std::string module::getName() const
{
    return m_name;
}

int module::getState() const
{
    return m_state;
}

bool module::isRunning() const
{
    return m_running;
}

receiver::receiver(const std::string& name)
    : module(receiverType, name)
{}

std::vector<color> receiver::getValidColors() const
{
    return m_validColors;
}

void receiver::setValidColors(std::vector<color> cols)
{
    m_validColors = cols;
}

void receiver::setWaitingColor(color c)
{
    m_waitingColor = c;
}

color receiver::getWaitingColor() const
{
    return m_waitingColor;
}

sorter::sorter(const std::string& name, int storageNum, int storages)
    : module(sorterType, name),
    m_storageNum(storageNum),
    m_bricks(0),
    m_storages(storages)
{}

void sorter::addBrick()
{
    m_bricks++;
}

void sorter::removeBrick()
{
    m_bricks--;
    if(m_bricks < 0)
    {
        m_bricks = 0;
    }
}

int sorter::getBricks() const
{
    return m_bricks;
}

bool sorter::isRunning() const
{
    int state = getState();
    return state == ms_started
    || state == ms_handlingBrick
    || state == ms_handledBrick
    || state == ms_waitingRunning;
}

int sorter::getStorages() const
{
    return m_storages;
}

void sorter::setBricks(int bricks)
{
    m_bricks = bricks;
}

void sorter::setWaitingStorage(int ws)
{
    m_waitingStorage = ws;
}

int sorter::getWaitingStorage() const
{
    return m_waitingStorage;
}

void sorter::setBrickColor(color c)
{
    m_brickColor = c;
}

color sorter::getBrickColor() const
{
    return m_brickColor;
}

storage::storage(const std::string& name, int capacity, int number)
    : module(storageType, name),
    m_wantedColor(unknown),
    m_capacity(capacity),
    m_stored(0),
    m_number(number)
{}

bool storage::isFull() const
{
    return m_stored >= m_capacity;
}

color storage::getColor() const
{
    return m_wantedColor;
}

int storage::getNumber() const
{
    return m_number;
}

void storage::setBrick()
{
    m_stored++;
}

void storage::setColor(color c)
{
    m_wantedColor = c;
}

bool storage::isBusy() const
{
    int state = getState();
    return state != ms_stopped;
}

void storage::empty()
{
    m_stored = 0;
}

int storage::getCapacity() const
{
    return m_capacity;
}

int storage::getStored() const
{
    return m_stored;
}
