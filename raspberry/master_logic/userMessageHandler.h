#ifndef PG26_USERMESSAGEHANDLER_H
#define PG26_USERMESSAGEHANDLER_H

#include <string>
#include "message.h"
#include "messageHandlerBase.h"
#include "userMessage.h"
#include "modules.h"
#include <algorithm>
#include <regex>
#include "mqttClientRaspberry.h"

class userMessageHandler : public messageHandlerBase<userMessage>
{
public:
    userMessageHandler(mqttClientRaspberry* mqttClient);

    ~userMessageHandler();

    bool hasNewMessages();

    userMessage getMessage();

    void sendReceiverStatus(receiver* r);
    void sendSorterStatus(sorter* s);
    void sendStorageStatus(storage* s);
    void sendStorageFull(storage* s);
    void sendSystemStatus(bool running);

private:
    void sendMessage(std::string msg);

    void msgParser(std::string msg);

    userMessageType parseUserMessageType(std::string strType);
};



#endif
