#ifndef PG26_MQTTCLIENT_RASPBERRY_H
#define PG26_MQTTCLIENT_RASPBERRY_H

#include <functional>
#include <thread>
#include <atomic>
#include <mosquittopp.h>

// libmosquitto namespace is named differently on raspberry pi
#ifdef __arm__
    #define LIBMOSQUITTO_NAMESPACE mosqpp
#else
    #define LIBMOSQUITTO_NAMESPACE mosquittopp
#endif



class mqttClientRaspberry : public LIBMOSQUITTO_NAMESPACE::mosquittopp
{
public:
    mqttClientRaspberry(const char* listenTopic);

    ~mqttClientRaspberry();

    typedef std::function<void(const std::string&, uint8_t*, uint32_t)> systemCallback_t;
    typedef std::function<void(std::string)> userCallback_t;

    void setSystemCallbackFn(systemCallback_t fn);

    void setUserCallbackFn(userCallback_t fn);

    void startReceiving();

    //<summary>Status of the connection. True if everything is in order</summary>
    bool status();

    void on_connect(int rc);

    void on_message(const struct mosquitto_message *message);

private:

    void receiverFunction();

    bool m_connected;
    systemCallback_t m_systemCallback;
    userCallback_t m_userCallback;

    std::atomic<bool> m_receiving;
    std::thread m_recvThread;
};




#endif
