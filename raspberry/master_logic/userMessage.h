#ifndef PG26_USERMESSAGE_H
#define PG26_USERMESSAGE_H

#include<map>
#include<string>
#include "json.hpp"

enum userMessageType
{
    umsg_receiverStatus = 1,
    umsg_sorterStatus = 2,
    umsg_storageStatus = 3,
    umsg_startSystem = 4,
    umsg_stopSystem = 5,
    umsg_setStorageColors = 6,
    umsg_storageFull = 7,
    umsg_storageEmpty = 8,
    umsg_sorterEmpty = 9,
    umsg_receiverEndWait = 10,
    umsg_sorterEndWait = 11,
    umsg_systemStatus = 12
};

struct userMessage
{
    userMessageType type;
    nlohmann::json data;
};

#endif
