#ifndef PG26_PARAMETERS_H
#define PG26_PARAMETERS_H


//#define MQTT_HOST_IP "127.0.0.1"
#define MQTT_HOST_IP "192.168.42.1"
#define MQTT_HOST_PORT 1883
#define MODULE_NAME_MAX_SIZE 16
#define MODULE_TOPIC_MAX_SIZE 16
#define MSG_SIZE 6
#define MSG_MAX_RESENDS 8
#define MSG_RESEND_DELAY 5000 // milliseconds


enum moduleType
{
    receiverType = 1,
    sorterType = 2,
    storageType = 3,
    masterType = 4
};


#endif
