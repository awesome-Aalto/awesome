#include <iostream>
#include <string>
#include "parameters.h"
#include "mqttClientRaspberry.h"
#include <chrono>



mqttClientRaspberry::mqttClientRaspberry(const char* listenTopic)
    : mosquittopp(("master"+std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count())).c_str()),
    m_connected(false),
    m_systemCallback(false),
    m_userCallback(false),
    m_receiving(false)
{
    // mqtt-lib initializations
    mosquitto_lib_init();

    std::cout << "Connecting to broker: " << MQTT_HOST_IP << std::endl;
    int x = mosquittopp::connect(MQTT_HOST_IP);
    std::cout << "Connect returned: " << (x==MOSQ_ERR_SUCCESS ? "ok" : "error") << std::endl;
    mosquittopp::subscribe(NULL, listenTopic);
}


mqttClientRaspberry::~mqttClientRaspberry()
{
    // wait until the receiving thread ends
    m_receiving = false;
    if (m_recvThread.joinable())
    {
        m_recvThread.join();
    }

    // mqtt-lib cleanup
    mosquitto_lib_cleanup();
}


void mqttClientRaspberry::setSystemCallbackFn(systemCallback_t fn)
{
    m_systemCallback = fn;
}

void mqttClientRaspberry::setUserCallbackFn(userCallback_t fn)
{
    m_userCallback = fn;
}

void mqttClientRaspberry::startReceiving()
{
    if (!m_receiving)
    {
        m_receiving = true;
        m_recvThread = std::thread(&mqttClientRaspberry::receiverFunction, std::ref(*this));
    }
}


//<summary>Status of the connection. True if everything is in order</summary>
bool mqttClientRaspberry::status()
{
    return m_connected && m_receiving;
}



void mqttClientRaspberry::on_connect(int rc)
{
    (void)rc;
    m_connected = true;
}


void mqttClientRaspberry::on_message(const struct mosquitto_message *message)
{
    std::string topic(message->topic);

    // accept only messages that has 'send/' prefix
    if (topic.size() > 5)
    {
        if (topic.substr(0,5) != "send/")
            return;

        // clip 'send/'
        topic = topic.substr(5);

        if (topic == "user")
        {
            if (m_userCallback)
            {
                m_userCallback(std::string((char*)message->payload));
            }
        }
        else
        {
            // assume system message
            if (m_systemCallback)
            {
                m_systemCallback(topic, (unsigned char*)(message->payload), message->payloadlen);
            }
        }
    }
}


void mqttClientRaspberry::receiverFunction()
{
    while (m_receiving)
    {
        mosquittopp::loop();
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}
