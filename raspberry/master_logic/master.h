#ifndef PG26_MASTER_H
#define PG26_MASTER_H

#include <thread>
#include <mutex>
#include <atomic>
#include <queue>
#include "systemMessageHandler.h"
#include "mqttClientRaspberry.h"
#include "modules.h"
#include <string>
#include "userMessage.h"
#include "userMessageHandler.h"
#include "color.h"

struct sender
{
    moduleType type;
    const std::string name;

    sender(moduleType t, const std::string& n) :
        type(t),
        name(n)
    {}
};

class master
{
public:
    master();

    ~master();

    // These are given from command line
    enum userCommand
    {
        CMD_UNKNOWN = 0,
        CMD_START = 1,
        CMD_QUIT = -1,
        CMD_STOP = 2,
        CMD_WAIT_RECEIVER_RUN = 3,
        CMD_STOP_RECEIVER_WAIT = 4
    };

    void run(const bool& running, bool user_commands);

private:
    void handleMessages();
    void handleUserMessages();
    void updateStatusToUI();
    sender* parseSender(std::string sndr);

    //message handlers
    void ok(msg_ok* msg, sender* sndr);
    void connected(msg_connected* msg, sender* sndr);
    void init(msg_init* msg, sender* sndr);
    void stop(msg_stop* msg, sender* sndr);
    void wait(msg_wait* msg, sender* sndr);
    void error(msg_error* msg, sender* sndr);
    void colorQuery(msg_colorQuery* msg, sender* sndr);
    void queryOk(msg_queryOk* msg, sender* sndr);
    void brickAccepted(msg_brickAccepted* msg, sender* sndr);
    void storageQuery(msg_storageQuery* msg, sender* sndr);
    void brickPushed(msg_brickPushed* msg, sender* sndr);
    void startConveyor(msg_startConveyor* msg, sender* sndr);
    void stopConveyor(msg_stopConveyor* msg, sender* sndr);
    void ready(msg_ready* msg, sender* sndr);
    void brickStored(msg_brickStored* msg, sender* sndr);
    void finalPosition(msg_finalPosition* msg, sender* sndr);
    void goInitialPosition(msg_goInitialPosition* msg, sender* sndr);
    void sendOk(sender* sndr);
    void receiverOk(sender* sndr);
    void sorterOk(sender* sndr);
    void storageOk(sender* sndr);
    void disconnected(msg_disconnected* msg, sender* sndr);

    void startSystem();
    void stopSystem();
    void startModule(module* m);
    void stopModule(module* m);
    void moduleWaitRunning(module* m);
    void moduleWaitHandling(module* m);
    void waitModule(module* m, moduleState ms);
    void emptyStorage(userMessage msg);
    void setStorageColors(userMessage msg);
    int parseIntFromMsg(userMessage msg, std::string field);
    void updateReceiverAcceptedColors(receiver* rcvr);
    bool receiverEndWait();
    void sorterEndWait();
    bool roomForColor(color c);

    storage* getStorage(int number);
    storage* getStorage(unsigned char number);
    storage* getStorage(std::string name);
    sorter* getSorter(bool dgb = true);
    receiver* getReceiver(bool dgb = true);
    void removeStorage(std::string name);
    void emptySorter();

    void askUserInput();
    std::atomic<bool> m_running;
    std::thread m_userThread;
    std::queue<userCommand> m_userCmds;
    std::mutex m_msgLock;
    std::mutex m_userCmdLock;

    // containers for module pointers
    std::vector<receiver*> m_receivers;
    std::vector<sorter*> m_sorters;
    std::vector<storage*> m_storages;

    mqttClientRaspberry m_mqttClient;
    systemMessageHandler m_systemMsgHandler;
    userMessageHandler m_userMsgHandler;
    //
    color debug_storageColor;
    color debug_getStorageColor();

    //
    bool m_systemRunning;
};


#endif
