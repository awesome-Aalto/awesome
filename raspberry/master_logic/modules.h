#ifndef PG26_MODULES_H
#define PG26_MODULES_H

#include <string>
#include <vector>

#include "parameters.h"
#include "color.h"

enum moduleState
{
    ms_error = -10,
    ms_unknown = 0,
    ms_connected = 10,
    ms_initializing = 15,
    ms_initialized = 20,
    ms_starting = 25,
    ms_started = 30,
    ms_stopping = 35,
    ms_stopped = 40,
    ms_handlingBrick = 45,
    ms_handledBrick = 50,
    ms_full = 60,
    ms_waitingRunning = 1005,
    ms_waitingHandling = 1015
};


class module
{
public:
    module(moduleType m, const std::string& name);
    void setState(moduleState newState);
    std::string getName() const;
    int getState() const;
    bool isRunning() const;
private:
    moduleType m_type;
    bool m_running;
    const std::string m_name;
    int m_state;
};


class receiver : public module
{
public:
    receiver(const std::string& name);

    void setColor(const color c);
    bool isValidColor(const color c) const;
    std::vector<color> getValidColors() const;
    void setValidColors(std::vector<color> cols);
    color getWaitingColor() const;
    void setWaitingColor(color c);
private:
    std::vector<color> m_validColors;
    color m_waitingColor;
};


class sorter : public module
{
public:
    sorter(const std::string& name, int storageNum, int storages);
    void addBrick();
    void removeBrick();
    int getBricks() const;
    bool isRunning() const;
    int getStorages() const;
    void setBricks(int bricks);
    int getWaitingStorage() const;
    void setWaitingStorage(int ws);
    color getBrickColor() const;
    void setBrickColor(color c);
private:
    int m_storageNum;
    std::vector<bool> m_storageSlots;
    int m_bricks;
    int m_storages;
    int m_waitingStorage;
    color m_brickColor;
};


class storage : public module
{
public:
    storage(const std::string& name, int capacity, int number);

    void setBrick();
    bool isFull() const;
    color getColor() const;
    int getNumber() const;
    void setColor(color c);
    bool isBusy() const;
    void empty();
    int getCapacity() const;
    int getStored() const;
private:
    color m_wantedColor;
    int m_capacity;
    int m_stored;
    int m_number;
};


#endif
