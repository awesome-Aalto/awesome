#include "mqttClientRaspberry.h"
#include "systemMessageHandler.h"

using time_point = std::chrono::system_clock::time_point;
using system_clock = std::chrono::system_clock;
using milliseconds = std::chrono::milliseconds;

systemMessageHandler::systemMessageHandler(mqttClientRaspberry* mqttClient)
    : messageHandlerBase(mqttClient),
    m_messageId(0),
    m_sending(false)
{
    m_mqttClient->setSystemCallbackFn(std::bind(&systemMessageHandler::msgParser, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    m_mqttClient->startReceiving();
    //start sending thread
    m_sending = true;
    m_senderThread = std::thread(&systemMessageHandler::handleOutboundMessages, std::ref(*this));
}


systemMessageHandler::~systemMessageHandler()
{
    while (hasNewMessages())
    {
        systemMessage msg = getMessage();
        delete msg.msg;
    }

    // wait until the receiving thread ends
    m_sending = false;
    if (m_senderThread.joinable())
    {
        m_senderThread.join();
    }
}


bool systemMessageHandler::hasNewMessages()
{
    m_queueLock.lock();
    bool b = !m_msgQueue.empty();
    m_queueLock.unlock();
    return b;
}


systemMessage systemMessageHandler::getMessage()
{
    m_queueLock.lock();
    systemMessage msg = m_msgQueue.front();
    m_msgQueue.pop();
    m_queueLock.unlock();
    return msg;
}


void systemMessageHandler::setReceiver(const std::string& name)
{
    m_sendTopic = "listen/" + name;
}


void systemMessageHandler::msgParser(const std::string name, uint8_t* buf, uint32_t len)
{
    message* msg = parseMessage(buf, len);
    purgeSendQueue(msg);
    systemMessage sysMsg = {name, msg};
    printInboundMsg(name, buf);
    m_queueLock.lock();
    m_msgQueue.push(sysMsg);
    m_queueLock.unlock();
    // msg needs to be deleted after use since it is created with "new"
    // should be handled in handleMessages in master.cpp
}


void systemMessageHandler::sendMessage(messageType type, bool answerRequired, unsigned char param1, unsigned char param2)
{
    // send buffer functionality
    outboundMessage m;
    m.topic = m_sendTopic;
    m.sendTime = system_clock::now();
    m.timesSent = 0;
    m.buffer[0] = getMessageId();
    m.buffer[1] = (uint8_t)type;
    m.buffer[2] = (answerRequired ? 1 : 0);
    m.buffer[3] = param1;
    m.buffer[4] = param2;

    if(answerRequired)
    {
        m_sendQueueLock.lock();
        m_msgSendQueue.push_back(m);
        m_sendQueueLock.unlock();
    }
    else
    {
        m_mqttClient->publish(NULL, m.topic.c_str(), 5, m.buffer);
        m.timesSent++;
        printOutboundMsg(m);
        std::cout << "no answer required." << std::endl;
        std::cout << std::endl;
    }
}

unsigned char systemMessageHandler::getMessageId()
{
    m_messageId++;
    if(m_messageId == 0)
    {
        m_messageId = 1;
    }
    return m_messageId;
}

void systemMessageHandler::handleOutboundMessages()
{
    while(m_sending)
    {
        m_sendQueueLock.lock();
        for(auto &msg : m_msgSendQueue)
        {
            time_point now = system_clock::now();
            if(msg.timesSent < MSG_MAX_RESENDS && (msg.timesSent == 0 || std::chrono::duration_cast<milliseconds>(now - msg.sendTime).count() > MSG_RESEND_DELAY))
            {
                m_mqttClient->publish(NULL, msg.topic.c_str(), 5, msg.buffer);
                msg.sendTime = system_clock::now();
                msg.timesSent++;
                printOutboundMsg(msg);
            }
            /*
            if(msg.timesSent == (MSG_MAX_RESENDS - 1))
            {
                std::cout << "message resends limit reached for message:" << std::endl;
                printOutboundMsg(msg);
            }
            */
        }
        m_sendQueueLock.unlock();

        std::this_thread::sleep_for(milliseconds(10));
    }
}

void systemMessageHandler::purgeSendQueue(message* msg)
{
    if(msg == nullptr) return;
    int idx = 0;
    bool found = false;
    outboundMessage* print;
    m_sendQueueLock.lock();
    for(auto &sent : m_msgSendQueue)
    {
        if(sent.buffer[0] == msg->getId())
        {
            print = &sent;
            found = true;
            break;
        }
        idx++;
    }
    if(found)
    {
        std::cout << "got answer, deleting message ";
        printBuffer(print->buffer);
        std::cout << std::endl;
        std::cout << std::endl;
        m_msgSendQueue.erase(m_msgSendQueue.begin()+idx);
    }
    m_sendQueueLock.unlock();
}

void systemMessageHandler::printOutboundMsg(outboundMessage msg)
{
    std::cout << std::endl;
    std::cout << "msg ";
    printBuffer(msg.buffer);
    std::cout << " to " << msg.topic << " sent " << msg.timesSent << " times " << std::endl;
    std::cout << std::endl;
}

void systemMessageHandler::printBuffer(uint8_t* buffer)
{
    std::cout << buffer[0]+0 << " ";
    printMsgType(buffer[1]);
    std::cout << " (" << buffer[2]+0 << ") " << buffer[3]+0 << " " << buffer[4]+0;
}

void systemMessageHandler::printInboundMsg(std::string name, uint8_t* buf)
{
    std::cout << std::endl;
    std::cout << "got system message from " << name << ": ";
    printBuffer(buf);
    std::cout << std::endl;
    std::cout << std::endl;
}

void systemMessageHandler::printMsgType(int msgType) const
{
    std::string msgTypeStr = "";
    if (msgType == 1)
        msgTypeStr = "ok";
    else if (msgType == 2)
        msgTypeStr = "connected";
    else if (msgType == 3)
        msgTypeStr = "initialize";
    else if (msgType == 4)
        msgTypeStr = "stop";
    else if (msgType == 5)
        msgTypeStr = "wait";
    else if (msgType == 6)
        msgTypeStr = "error";
    else if (msgType == 7)
        msgTypeStr = "colorQuery";
    else if (msgType == 8)
        msgTypeStr = "queryOK";
    else if (msgType == 9)
        msgTypeStr = "brickAccepted";
    else if (msgType == 10)
        msgTypeStr = "storagePushed";
    else if (msgType == 11)
        msgTypeStr = "brickPushed";
    else if (msgType == 12)
        msgTypeStr = "startConveyor";
    else if (msgType == 13)
        msgTypeStr = "stopConveyor";
    else if (msgType == 14)
        msgTypeStr = "ready";
    else if (msgType == 15)
        msgTypeStr = "brickStored";
    else if (msgType == 16)
        msgTypeStr = "finalPosition";
    else if (msgType == 17)
        msgTypeStr = "goInitialPosition";
    else if (msgType == 18)
        msgTypeStr = "disconnected";
    std::cout << msgTypeStr;
}
