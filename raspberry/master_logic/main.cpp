#include <iostream>
#include <csignal>
#include <cstring>

#include "master.h"


bool running = true;

void signalHandler(int)
{
    running = false;
}


int main(int argc, char *argv[])
{
    // Check if user command mode selected
    bool user_commands = false;
    if (argc > 1 && !strncmp(argv[1], "-u", 2))
    {
        user_commands = true;
    }

    // Initialize signal handling
    std::signal(SIGINT, signalHandler);

    std::cout << "Master running" << std::endl;
    master m;
    m.run(running, user_commands);
    std::cout << "Master stopping" << std::endl;
}
