from app import app, socketio
from flask import render_template
from flask_socketio import emit
import uuid
import paho.mqtt.client as mqtt


# global mqtt-client instance
client = mqtt.Client(client_id=str(uuid.uuid4()), protocol='MQTTv311', clean_session=True)
thread = None
messages = [
"ok",
"connected",
"init",
"stop",
"wait",
"error",
"colorQuery",
"queryOk",
"brickAccepted",
"storageQuery",
"brickPushed",
"startConveyor",
"stopConveyor",
"ready",
"brickStored",
"finalPosition",
"goInitialPosition",
"disconnected"]

# decorators for different paths to be rendered
@app.route('/')
@app.route('/index')
def reactpanel():
    return render_template('reactpanel.html', async_mode=socketio.async_mode)

@app.route('/debug')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)


# websocket message handlers
@socketio.on('connect')
def connect():
    global thread
    if thread is None:
        # start background thread for mqtt-message receiving
        thread = socketio.start_background_task(target=serverthread)

@socketio.on('mqtt_sender')
def mqtt_sender(msg):
    global client
    topic = 'listen/' + msg['rx']
    try:
        raw = [1, int(msg['type']), int(msg['ack']), int(msg['par1']), int(msg['par2'])]
        client.publish(topic, bytearray(raw))
    except ValueError:
        client.publish('error', 'invalid values')

@socketio.on('userMessage')
def userMessage(msg):
    global client
    topic = 'send/user'
    try:
        client.publish(topic, msg)
    except ValueError:
        client.publish('error', 'invalid values')


# emit mqtt debug messages to browser log view
def logger(msg):
    socketio.emit('mqttlog', {'data':msg})


# emit mqtt messages to the control panel
def inboundMessage(msg):
    socketio.emit('inbound', {'data':msg})


# background function to start and maintain mqtt connection
def serverthread():

    # connection callback
    def on_connect(client, userdata, rc):
        # listen all channels
        client.subscribe('#')

    # incoming message handler callback
    def on_message(client, userdata, msg):
        if msg.payload is None:
            logger('Topic: ' + msg.topic + ' -- Message: None')
        elif 'ui' in msg.topic:
            inboundMessage(msg.payload)
        elif 'user' not in msg.topic:
            p = bytearray(msg.payload)
            idx = msg.topic.index("/")
            beg = msg.topic[0:idx]
            if beg == "send":
                beg += "__"
            end = msg.topic[idx+1:]
            if end == "sorter":
                end += "__"
            topaz = beg+"/"+end
            #logger('Topic: ' + topaz + ' -- Message: ' + ",".join(map(lambda b: format(b, "d"), p)) + ' -- Length: ' + str(len(p)))
            logger('Topic: ' + topaz + ' -- Message: ' + format(p[0], "d") + ","+ messages[p[1]-1] + ","+format(p[2], "d") + ","+format(p[3], "d") + "," + format(p[4], "d"))

    # attach callbacks
    client.on_connect = on_connect
    client.on_message = on_message

    # connect to broker
    client.connect('192.168.42.1', 1883, 60)
    while True:
        # give time to socketio functions
        socketio.sleep(0.1)
        # maintain mqtt connection and receive messages
        client.loop(0.1)
