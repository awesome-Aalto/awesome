function printStatus(state)
{
    switch(state)
    {
        case -1:
            return "error";
        case 0:
            return "disconnected";
        case 10:
            return "connected";
        case 15:
            return "initializing";
        case 20:
            return "initialized";
        case 25:
            return "starting";
        case 30:
            return "started";
        case 35:
            return "stopping";
        case 40:
            return "stopped";
        case 45:
            return "handling brick";
        case 50:
            return "handled brick";
        case 60:
            return "full";
        case 1005:
            return "waiting while running";
        case 1015:
            return "waiting for handling";

    }
}

function printColor(col)
{
    if(typeof col === "string")
    {
        col = parseInt(col);
    }
    switch(col)
    {
        case 1:
            return "red";
        case 2:
            return "green";
        case 3:
            return "blue";
        case 4:
            return "yellow";
        case 5:
            return "cyan";
        case 6:
            return "magenta";
        case 7:
            return "white";
        case 8:
            return "black";
        case 100:
            return "unknown";
    }
}
