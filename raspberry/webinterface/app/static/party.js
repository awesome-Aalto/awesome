function startParty(){
    clippy.load('Clippy', function(agent) {
        // Do anything with the loaded agent
        agent.show();
        //agent.moveTo(100,100);
        agent.speak('When all else fails, Moomin music saves! Welcome to the party mode!')
        agent.play("Hearing_1");
        /*
        setInterval(function(){
            agent.moveTo(Math.random()*1000, Math.random()*1000);
        },1000);
        */
    });
    for (var i = 0, nodes = document.querySelectorAll('*'); i < nodes.length; i++) {
        $(nodes[i]).attr("data-oldcolor", $(nodes[i]).css("backgroundColor"));
    }
    partyLights = setInterval(function(){
      for (var i = 0, nodes = document.querySelectorAll('*'); i < nodes.length; i++) {
          nodes[i].style.backgroundColor = '#' + Math.floor(Math.random()*16777215).toString(16);
          $(".dancer").css("background-color", "transparent");
          $("#dancercontainer").css("background-color", "transparent");
      }
  }, 50);

    document.getElementById('muumimusa').play();

    d1 = dancer("i1", "i2");
    d2 = dancer("i3", "i4");
    d3 = dancer("i5", "i6");
    d4 = dancer("i7", "i8");
    d5 = dancer("i9", "i10");
    //d6 = dancer("i11", "i12");
}

function stopParty(){
    clearInterval(partyLights);
    clearInterval(d1);
    clearInterval(d2);
    clearInterval(d3);
    clearInterval(d4);
    clearInterval(d5);
    //clearInterval(d6);
    clearDancer("i1", "i2");
    clearDancer("i3", "i4");
    clearDancer("i5", "i6");
    clearDancer("i7", "i8");
    clearDancer("i9", "i10");
    document.getElementById('muumimusa').currentTime = 0;
    document.getElementById('muumimusa').pause();
    for (var i = 0, nodes = document.querySelectorAll('*'); i < nodes.length; i++) {
        nodes[i].style.backgroundColor = $(nodes[i]).attr("data-oldcolor");;
        $(nodes[i]).attr("data-oldcolor", "");
    }
}

function dancer(img1, img2)
{
    var firstImg = true;
    return setInterval(function(){
        var showd = firstImg ? img1 : img2;
        var noshow = firstImg ? img2 : img1;
        var elem = document.getElementById(showd);
        elem.style.top = "200px";
        elem.style.left = "50px";
        $("#"+showd).css("display", "initial");
        $("#"+noshow).hide("display", "none");
        firstImg = !firstImg;
    }, 350);
}

function clearDancer(img1, img2)
{
    $("#"+img1).hide();
    $("#"+img2).hide();
}
