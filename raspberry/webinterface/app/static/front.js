class ControlsContainer extends React.Component {
    render(){
        return(
            <div id="controlsContainer">
                <h2>Controls</h2>
                <div id="masterControls">
                    <button id="startSystem" className="control">Start the system</button>
                    <button id="stopSystem" className="control">Stop the system</button>
                    <button id="emptySorter" className="control">Empty sorter</button>
                    <button id="receiverEndWait" className="control">End receiver wait</button>
                    <button id="sorterEndWait" className="control">End sorter wait</button>
                    <button id="debugView" className="control">Debug view</button>
                    <button id="keijo" className="control">Motivation boost</button>
                    <button id="party" className="control rainbow">
                        <div>
                            <span className="">
                                PARTY MODE
                            </span>
                        </div>
                    </button>

                </div>
                <div id="debugContainer" data-visible="0">
                    <h2>Message log</h2>
                    <div id="debugDataContainer"></div>
                </div>
            </div>
        );
    }
    componentDidMount(){
        var partyModeOn = false;
        var partyLights;

        $("#startSystem").click(function(){
            socket.emit('userMessage', userMessage("4",{ description: "useless body" }));
            //$("#stopSystem").prop("disabled", false);
            //$("#startSystem").prop("disabled", true);
        });

        $("#stopSystem").click(function(){
            socket.emit('userMessage', userMessage("5", { description: "useless body" }));
            //$("#stopSystem").prop("disabled", true);
            //$("#startSystem").prop("disabled", false);
        });

        $(".emptyStorage").click(function(){
            socket.emit('userMessage', userMessage("8", { number : $(this).attr("data-id") }));
        });

        $("#emptySorter").click(function(){
            socket.emit('userMessage', userMessage("9", { description: "useless body" }));
        });

        $("#receiverEndWait").click(function(){
            socket.emit('userMessage', userMessage("10", { description: "useless body" }));
        });

        $("#sorterEndWait").click(function(){
            socket.emit('userMessage', userMessage("11", { description: "useless body" }));
        });

        $("#debugView").click(function(){
            var isVisible = $("#debugContainer").attr("data-visible") == "1";
            if(!isVisible){
                $("#debugContainer").css("visibility", "visible");
                $("#debugContainer").attr("data-visible", "1");
            }else{
                $("#debugContainer").css("visibility", "hidden");
                $("#debugContainer").attr("data-visible", "0")
            }
        });

        $('#party').click(function() {
            if(!partyModeOn){
                startParty();
                partyModeOn = true;
            }else{
                stopParty();
                partyModeOn = false;
            }
        });

        $("#keijo").click(function(){
            document.getElementById('awesome').play();
            $("#keijopapukaija").css("display", "initial");
            setTimeout(function(){
                $("#keijopapukaija").css("display", "none");
            }, 2000);
        });
    }
}

class Receiver extends React.Component{
    constructor(){
        super();
        this.state = {
            name: "unknown",
            status: 0,
            running: false,
            accepts: Array(3).fill(100),
            cln: "disconnected"
        };
        socket.on('inbound', function(msg) {
            var obj = JSON.parse(msg.data);
            if(obj.type == 1)
            {
                var msgobj = JSON.parse(obj.message);
                this.setState({
                    status: msgobj.state,
                    name: msgobj.name,
                    cln: msgobj.state == 0 ? "disconnected" : "connected",
                    accepts: msgobj.colors
                });
            }
        }.bind(this));
    }
    render(){
        var acceptedColors = "";
        var first = true;
        for (var i = 0; i < this.state.accepts.length; i++) {
            if(!first){
                acceptedColors += ", ";
            }
            acceptedColors += printColor(this.state.accepts[i]);
            first = false;
        }
        var baseClass = "moduleContainer ";
        return(
            <div id="receiverContainer" className={baseClass + this.state.cln}>
                <div id="receiverInfo">
                    <b>{this.state.name}</b><br/>
                    Status: {printStatus(this.state.status)}<br/>
                    Accepts: {acceptedColors}
                </div>
            </div>
        );
    }
};

class Sorter extends React.Component {
    constructor(){
        super();
        this.state = {
            name: "unknown",
            status: 0,
            bricks: 0,
            storages: 0,
            cln: "disconnected"
        };
        socket.on('inbound', function(msg) {
            var obj = JSON.parse(msg.data);
            if(obj.type == 2)
            {
                var msgobj = JSON.parse(obj.message);
                this.setState({
                    status: msgobj.state,
                    name: msgobj.name,
                    storages: msgobj.storages,
                    bricks: msgobj.bricks,
                    cln: msgobj.state == 0 ? "disconnected" : "connected"
                });
            }
        }.bind(this));
    }
    render(){
        var baseClass = "moduleContainer ";
        return(
            <div id="sorterContainer" className={baseClass + this.state.cln}>
                <div id="sorterInfo">
                    <b>{this.state.name}</b><br/>
                    Status: {printStatus(this.state.status)}<br/>
                    Bricks: {this.state.bricks}
                </div>
            </div>
        );
    }
};

class Storage extends React.Component {
    constructor(){
        super();
        this.state = {
            name: "unknown",
            status: 0,
            capacity: 3,
            stored: 0,
            color: "unknown",
            full: false,
            cln: "disconnected"
        };
        socket.on('inbound', function(msg) {
            var obj = JSON.parse(msg.data);
            var msgobj = JSON.parse(obj.message);

            //storage status
            if(obj.type == 3 && msgobj.number == this.props.id)
            {
                this.setState({
                    status: msgobj.state,
                    name: msgobj.name,
                    color: msgobj.color,
                    full: msgobj.full,
                    cln: msgobj.state == 0 ? "disconnected" : "connected",
                    capacity: msgobj.capacity,
                    stored: msgobj.stored
                });
            }
            //storage full
            if(obj.type == 7 && msgobj.number == this.props.id)
            {
                alert("Storage " + this.props.id + " is full. Please empty the storage.");
            }
        }.bind(this));
    }
    handleChange(i)
    {
        socket.emit("userMessage", userMessage("6", { number: this.props.id, color: $("#colorSelect"+i).val() }));
    };
    render() {
        var baseClass = "moduleContainer storageContainer";
        var fullIndication = " ";
        if(this.state.status != 0){
            fullIndication = this.state.full ? " fullStorage " : " notfullStorage ";
        }
        return (
            <div id={"storage"+this.props.id+"Container"} className={baseClass + fullIndication + this.state.cln}>
                <div className="storageInfo">
                    <b>{this.state.name}</b><br/>
                    Status: {printStatus(this.state.status)}<br/>
                    Stored: {this.state.stored}/{this.state.capacity}<br/>
                    Color: {printColor(this.state.color)}<br/>
                    Full: {this.state.full ? "true" : "false"}
                </div>
                <br/>
                Change the color:<br/>
                <select id={"colorSelect"+this.props.id} onChange={() => this.handleChange(this.props.id)}>
                    <option value="100"></option>
                    <option value="1">red</option>
                    <option value="2">green</option>
                    <option value="3">blue</option>
                    <option value="4">yellow</option>
                    <option value="6">magenta</option>
                </select>
                <br/>
                <button className="emptyStorage" data-id={this.props.id}>Empty storage</button>
            </div>
        );
    }
};

class SystemContainer extends React.Component {
    constructor(){
        super();
        this.state = {
            running: false
        }
        socket.on('inbound', function(msg) {
            var obj = JSON.parse(msg.data);
            if(obj.type == 12)
            {
                var msgobj = JSON.parse(obj.message);
                this.setState({
                    running: msgobj.running
                });
            }
        }.bind(this));
    }
    render(){
        return(
            <div id="systemContainer">
                <h2>System status: {this.state.running ? "running" : "stopped"}</h2>
                <div className="row">
                    <Storage id="1" />
                    <Storage id="3" />
                </div>
                <div className="row">
                    <Receiver />
                    <Sorter />
                </div>
                <div className="row">
                    <Storage id="2" />
                </div>
            </div>
        );
    }
};

class MasterContainer extends React.Component {
    render() {
        return(
            <div id="masterContainer">
                <SystemContainer />
                <ControlsContainer />
            </div>
        );
    }
};
//
class ControlPanel extends React.Component {
    render() {
        return (
            <div id="controlPanel">
                <h1 id="mainHeader"><span id="awesome">AWESOME</span> Control panel! - <em className="smallheader">"Smoothly dynamic"</em><span className="smalltext">®©™</span></h1>

                <MasterContainer />
            </div>
        );
    }
};

ReactDOM.render(
    <ControlPanel />,
    document.getElementById('container')
);
