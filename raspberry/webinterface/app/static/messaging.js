function userMessage(t, msg){
    messageJSON =
    {
        type: t,
        message : msg
    };
    let str = JSON.stringify(messageJSON);
    console.log(str)
    return str;
};

function connect(){
    io.connect('http://' + document.domain + ':' + location.port,{transports:["polling"]});
}

socket.on('error', function(){
    console.log("error, reconnecting");
    connect();
});

socket.on("reconnect", function(){
    console.log("reconnected");
});

socket.on("reconnect_failed", function(){
    console.log("reconnect failed");
    connect();
});

socket.on("connect_error", function(){
    console.log("connection failed");
    connect();
});

socket.on("connect", function(){
    console.log("connected!");
});

socket.on('inbound', function(msg) {
    //console.log(msg);
    var obj = JSON.parse(msg.data);
    var name = "unknown";
    var classname = name;
    var d = "";
    switch(obj.type){
        case 1:
            name = "receiver";
            classname = name;
            var mo = JSON.parse(obj.message);
            d = "state: " + mo.state + " accepts: " + mo.colors;
            break;
        case 2:
            name = "sorter";
            classname = name;
            var mo = JSON.parse(obj.message);
            d = "state: " + mo.state + " storages: " + mo.storages + " bricks: " + mo.bricks;
            break;
        case 3:
            var mo = JSON.parse(obj.message);
            name = "storage";
            classname = name;
            name += mo.number;
            d = "state: " + mo.state + " color: " + mo.color + " bricks: " + mo.stored + "/" + mo.capacity;
            break;
    }
    $('#debugDataContainer').prepend("<div class='"+classname+"Msg "+name+"Msg'><div class='debugname'>"+name+":</div><div class='debugdata'>" + d + "</div></div>");
});
