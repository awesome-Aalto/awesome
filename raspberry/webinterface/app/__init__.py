#from gevent import monkey
#monkey.patch_all()

from flask import Flask
from flask_socketio import SocketIO

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret'
#socketio = SocketIO(app, async_mode="threading")
socketio = SocketIO(app)
from app import views
