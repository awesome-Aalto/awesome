#ifndef PG26_COLOR_H
#define PG26_COLOR_H

/**
 * Enum to contain different colors
 */
enum color {
    red = 1,
    green = 2,
    blue = 3,
    yellow = 4,
    cyan = 5,
    magenta = 6,
    white = 7,
    black = 8,
    unknown = 100
};

#endif
