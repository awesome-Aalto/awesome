#ifndef PG26_COLORSENSOR_H
#define PG26_COLORSENSOR_H

#include "color.h"
#include "SFE_ISL29125.h"
#include "Arduino.h"

/**
 * Stores RGB values
 */
class rgb{
public:
    rgb(float r, float g, float b);
    /**
     * Get red channel
     * @return red channel
     */
    float getR();
    /**
     * Get blue channel
     * @return blue channel
     */
    float getG();
    /**
     * Get green channel
     * @return green channel
     */
    float getB();
private:
    /**
     * red channel value
     */
    float m_r;
    /**
     * blue channel value
     */
    float m_g;
    /**
     * green channel value
     */
    float m_b;
};

/**
 * RGB Sensor class
 */
class colorSensor{
public:
    /**
     * constructor
     */
    colorSensor();
    /**
     * initialize the rgb sensor
     */
    bool init();
    /**
     * reads the rgb sensor and determines the color
     * @param  time  total measurement time
     * @param  count total number of measurements
     * @return       detected color
     */
    color getColor(int time, int count);
    /**
     * calibrates the sensor to current lighting environment
     * @param time  total measurement time
     * @param count total number of measurements
     */
    void calibrate(int time, int count);


private:
    /**
     * RGB sensor.
     */
    SFE_ISL29125 RGB_sensor;
    /**
     * Determines the color based on the measurements
     * @param  measurement measurement data
     * @return             color enum
     */
     color determineColor(rgb measurement);

     /**
      * Returns the "distance" between the given rgb valus and the given color
      * @param  r red value of the rgb value
      * @param  g green value of the rgb value
      * @param  b blue value of the rgb value
      * @param  c the color that the distance is calculated to
      * @return   the distance between the rgb value and the color
      */
     double getColorDistance(float r, float g, float b, color c);

    /**
     * Performs count measurements totaling time milliseconds
     * @param  time  total measurement time
     * @param  count total measurement count
     * @return       measurement RGB data
     */
    rgb measure(int time, int count);

    /**
     * red channel normalization value
     */
    int m_redCalib;
    /**
     * blue channel normalization value
     */
    int m_blueCalib;
    /**
     * green cahnnel normalization value
     */
    int m_greenCalib;

    double m_colorDistThresh;
};



#endif
