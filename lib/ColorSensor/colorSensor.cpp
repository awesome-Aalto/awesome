#include "colorSensor.h"


#pragma region color sensor

//constructor
colorSensor::colorSensor() :
m_colorDistThresh(10.0)
{}

//initialize the sensor
bool colorSensor::init()
{
    return RGB_sensor.init();
}

//this calibrates the sensor to the current lighting settings
//it measures the environment count times, and stores the averaged results
void colorSensor::calibrate(int time, int count)
{
    int rsum = 0;
    int gsum = 0;
    int bsum = 0;
    int tot = 0;
    float avg = 0;
    for(int i = 0; i < count; i++)
    {
        unsigned int r = RGB_sensor.readRed();
        unsigned int g = RGB_sensor.readGreen();
        unsigned int b = RGB_sensor.readBlue();
        rsum += r;
        gsum += g;
        bsum += b;
        delay(time/count);
    }

    tot = rsum+gsum+bsum;
    avg = tot/3;
    m_redCalib = rsum-avg;
    m_blueCalib = bsum-avg;
    m_greenCalib = gsum-avg;
}

//wrapper for measure and determine color
color colorSensor::getColor(int time, int count)
{
    rgb measurement = measure(time, count);
    return determineColor(measurement);
}

//this function performs the measurement
rgb colorSensor::measure(int time, int count)
{
    int rsum = 0;
    int gsum = 0;
    int bsum = 0;

    //read the sensor count times
    for(int i = 0; i < count; i++)
    {
        unsigned int r = RGB_sensor.readRed();
        unsigned int g = RGB_sensor.readGreen();
        unsigned int b = RGB_sensor.readBlue();
        rsum += r;
        gsum += g;
        bsum += b;
        delay(time/count);
    }

    //average and normalize
    float tot = rsum+gsum+bsum;
    float rv = (rsum-m_redCalib)/tot*100;
    float gv = (gsum-m_greenCalib)/tot*100;
    float bv = (bsum-m_blueCalib)/tot*100;

    //print
    Serial.print("Red Val: "); Serial.print(rv); Serial.println("%");
    Serial.print("Green Val: "); Serial.print(gv); Serial.println("%");
    Serial.print("Blue Val: "); Serial.print(bv); Serial.println("%");

    return rgb(rv,gv,bv);
}

//this determines the color of the measurement
color colorSensor::determineColor(rgb measurement)
{
    double closestDistance = 1.7E308;
    double distance = 0.0;
    color closestColor = unknown;

    float r = measurement.getR();
    float g = measurement.getG();
    float b = measurement.getB();

    for(int i = 1; i <= 8; i++)
    {
        //find the closest color to the given rgb values
        distance = getColorDistance(r,g,b,(color)i);
        if(distance < closestDistance)
        {
            closestDistance = distance;
            closestColor = (color)i;
        }
    }
    //check if the closest color is close enough to match with the rgb value
    if(closestDistance > m_colorDistThresh)
    {
        return unknown;
    }
    else
    {
        return closestColor;
    }
}

double colorSensor::getColorDistance(float r, float g, float b, color c)
{
    float rRef = 0.0;
    float gRef = 0.0;
    float bRef = 0.0;

    if(c == blue)
    {
        rRef = 21.0;
        gRef = 32.0;
        bRef = 47.0;
    }
    else if(c == red)
    {
        rRef = 42.0;
        gRef = 31.0;
        bRef = 26.0;
    }
    else if(c == green)
    {
        rRef = 26.0;
        gRef = 40.0;
        bRef = 33.0;
    }
    else if(c == yellow)
    {
        rRef = 37.0;
        gRef = 42.0;
        bRef = 21.0;
    }
    else if(c == magenta)
    {
        rRef = 28.0;
        gRef = 30.0;
        bRef = 41.0;
    }
    else if(c == black)
    {
        rRef = 33.33;
        gRef = 33.33;
        bRef = 33.33;
    }
    /*else if(c == white)
    {
        rRef = 28.0;
        gRef = 38.0;
        bRef = 35.0;
    }*/
    else
    {
        return 1.7E308;
    }
    return sqrt(pow(rRef - r, 2) + pow(gRef - g, 2) + pow(bRef - b, 2));
}

#pragma endregion

#pragma region rgb

//constructor
rgb::rgb(float r, float g, float b)
{
    m_r = r;
    m_g = g;
    m_b = b;
}

//get r channel
float rgb::getR()
{
    return m_r;
}

//get b channel
float rgb::getB()
{
    return m_b;
}

//get g channel
float rgb::getG()
{
    return m_g;
}

#pragma endregion
