#ifndef PG26_BUTTON_H
#define PG26_BUTTON_H

///Button class that takes care of button's debounce phenomena.
class button
{
public:
    ///<summary>Button class constructor</summary>
    ///<param name="pin">Pin in which the button is connected</param>
    ///<param name="debounceDelay">Delay in milliseconds that is allowed for the button signal to debounce until the value is read</param>
    ///<param name="name">Name of the button</param>
    button(uint8_t pin, uint8_t debounceDelay, String name);

    ///<summary>Read button's state (handles debouncing of the signal)</summary>
    int readButtonState();

    ///<summary>Prints button's data in the serial monitor</summary>
	void printData();

private:
    uint8_t m_pin;
    uint8_t m_debounceDelay;
    int m_lastDebounceTime;
    String m_name;
    uint8_t m_buttonState;
    uint8_t m_lastButtonState;
};

#endif
