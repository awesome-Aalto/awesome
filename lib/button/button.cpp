#include "Arduino.h"

#include "button.h"

button::button(uint8_t pin, uint8_t debounceDelay, String name) :
    m_pin(pin),
    m_debounceDelay(debounceDelay),
    m_name(name),
    m_lastDebounceTime(0),
    m_buttonState(LOW),
    m_lastButtonState(LOW)

{
    pinMode(m_pin, INPUT);
}

int button::readButtonState()
{
    //Read the state of the button
    uint8_t reading = digitalRead(m_pin);

    //Check to see if you just pressed the button
    //(i.e. the input went from LOW to HIGH),  and you've waited
    //long enough since the last press to ignore any noise

    //If the switch changed, due to noise or pressing
    if (reading != m_lastButtonState)
    {
        //Reset the debouncing timer
        m_lastDebounceTime = millis();
    }

    if ((millis() - m_lastDebounceTime) > m_debounceDelay)
    {
        //Whatever the reading is at, it's been there for longer
        //than the debounce delay, so take it as the actual current state

        //If the button state has changed:
        if (reading != m_buttonState)
        {
            m_buttonState = reading;
        }
    }

    //Save the reading.  Next time through the loop,
    //it'll be the lastButtonState:
    m_lastButtonState = reading;

    return m_buttonState;
};

void button::printData()
{
    Serial.print("Button name: ");
    Serial.println(m_name);
    Serial.print("  -Button pin is: ");
    Serial.println(m_pin, DEC);
    Serial.print("  -Button's debounce delay is: ");
    Serial.println(m_debounceDelay, DEC);
};
