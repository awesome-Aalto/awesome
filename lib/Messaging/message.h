#ifndef PG26_MESSAGE_H
#define PG26_MESSAGE_H


#include "color.h"
#include "parameters.h"

enum messageType {
    e_ok = 1,
    e_connected = 2,
    e_init = 3, //start the system
    e_stop = 4,
    e_wait = 5,
    e_error = 6,
    e_colorQuery = 7, //is this the right color, receiver
    e_queryOk = 8, //response to query
    e_brickAccepted = 9, //receiver response to brick
    e_storageQuery = 10, //sorter question of storage
    e_brickPushed = 11,
    e_startConveyor = 12,
    e_stopConveyor = 13,
    e_ready = 14,
    e_brickStored = 15,
    e_finalPosition = 16,
    e_goInitialPosition = 17,
    e_disconnected = 18
};

class message
{
public:
    message(unsigned char messageId, messageType type, bool answerRequired);

    virtual ~message();

    unsigned char getId() const;

    messageType getType() const;

    bool isAnswerRequired() const;

    virtual moduleType getModuleType() const {return (moduleType)0;}            // default implementation is empty

    virtual color getColor() const {return (color)0;}            // default implementation is empty

    virtual bool isOk() const {return false;}               // default implementation is empty

    virtual bool isAccepted() const {return false;}           // default implementation is empty

    virtual unsigned char getStorageNum() const {return 0;}     // default implementation is empty

    virtual bool isPushed() const {return false;}             // default implementation is empty

    virtual char getParameter() const {return 0;}

private:
    const unsigned char m_messageId;
    const messageType m_type;
    const bool m_answerRequired;
};

// message subtypes
class msg_ok : public message
{
public:
    msg_ok(unsigned char messageId, bool answerRequired);
private:

};

class msg_connected : public message
{
public:
    msg_connected(unsigned char messageId, bool answerRequired, moduleType modType, char parameter);

    moduleType getModuleType() const;

    char getParameter() const;

private:
    const moduleType m_moduleType;
    const char m_parameter;
};

class msg_init : public message
{
public:
    msg_init(unsigned char messageId, bool answerRequired);
private:

};

class msg_stop : public message
{
public:
    msg_stop(unsigned char messageId, bool answerRequired);
private:

};

class msg_wait : public message
{
public:
    msg_wait(unsigned char messageId, bool answerRequired);
private:

};

class msg_error : public message
{
public:
    msg_error(unsigned char messageId, bool answerRequired);
private:

};

class msg_colorQuery : public message
{
public:
    msg_colorQuery(unsigned char messageId, bool answerRequired, color c);

    color getColor() const;
private:
    const color m_color;
};

class msg_queryOk : public message
{
public:
    msg_queryOk(unsigned char messageId, bool answerRequired, bool isOk);

    bool isOk() const;
private:
    const bool m_isOk;
};

class msg_brickAccepted : public message
{
public:
    msg_brickAccepted(unsigned char messageId, bool answerRequired, bool accepted, color c);

    bool isAccepted() const;

    color getColor() const;
private:
    const bool m_accepted;
    const color m_color;
};

class msg_storageQuery : public message
{
public:
    msg_storageQuery(unsigned char messageId, bool answerRequired, color c, unsigned char storageNum);

    color getColor() const;

    unsigned char getStorageNum() const;
private:
    const color m_color;
    const unsigned char m_storageNum;
};

class msg_brickPushed : public message
{
public:
    msg_brickPushed(unsigned char messageId, bool answerRequired, bool pushed, unsigned char storageNum);

    bool isPushed() const;

    unsigned char getStorageNum() const;
private:
    const bool m_pushed;
    const int m_storageNum;
};

class msg_startConveyor : public message
{
public:
    msg_startConveyor(unsigned char messageId, bool answerRequired);
private:

};

class msg_stopConveyor : public message
{
public:
    msg_stopConveyor(unsigned char messageId, bool answerRequired);
private:

};

class msg_ready : public message
{
public:
    msg_ready(unsigned char messageId, bool answerRequired);
private:

};

class msg_brickStored : public message
{
public:
    msg_brickStored(unsigned char messageId, bool answerRequired);
private:

};

class msg_finalPosition : public message
{
public:
    msg_finalPosition(unsigned char messageId, bool answerRequired);
private:

};

class msg_goInitialPosition : public message
{
public:
    msg_goInitialPosition(unsigned char messageId, bool answerRequired);
private:

};

class msg_disconnected : public message
{
public:
    msg_disconnected(unsigned char messageId, bool answerRequired, moduleType modType, char parameter);

    moduleType getModuleType() const;

    char getParameter() const;

private:
    const moduleType m_moduleType;
    const char m_parameter;
};

#endif
