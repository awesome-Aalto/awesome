#include "message.h"

message::message(unsigned char messageId, messageType type, bool answerRequired) :
    m_messageId(messageId),
    m_type(type),
    m_answerRequired(answerRequired)
{

}

message::~message()
{

}

unsigned char message::getId() const
{
    return m_messageId;
}

messageType message::getType() const
{
    return m_type;
}

bool message::isAnswerRequired() const
{
    return m_answerRequired;
}

msg_ok::msg_ok(unsigned char messageId, bool answerRequired) :
    message(messageId, e_ok, answerRequired)
{

}

msg_connected::msg_connected(unsigned char messageId, bool answerRequired, moduleType modType, char parameter) :
    message(messageId, e_connected, answerRequired),
    m_moduleType(modType),
    m_parameter(parameter)
{

}

moduleType msg_connected::getModuleType() const
{
    return m_moduleType;
}

char msg_connected::getParameter() const
{
    return m_parameter;
}

msg_init::msg_init(unsigned char messageId, bool answerRequired) :
    message(messageId, e_init, answerRequired)
{

}

msg_stop::msg_stop(unsigned char messageId, bool answerRequired) :
    message(messageId, e_stop, answerRequired)
{

}

msg_wait::msg_wait(unsigned char messageId, bool answerRequired) :
    message(messageId, e_wait, answerRequired)
{

}

msg_error::msg_error(unsigned char messageId, bool answerRequired) :
    message(messageId, e_error, answerRequired)
{

}

msg_colorQuery::msg_colorQuery(unsigned char messageId, bool answerRequired, color c) :
    message(messageId, e_colorQuery, answerRequired),
    m_color(c)
{

}

color msg_colorQuery::getColor() const
{
    return m_color;
}

msg_queryOk::msg_queryOk(unsigned char messageId, bool answerRequired, bool isOk) :
    message(messageId, e_queryOk, answerRequired),
    m_isOk(isOk)
{

}

bool msg_queryOk::isOk() const
{
    return m_isOk;
}

msg_brickAccepted::msg_brickAccepted(unsigned char messageId, bool answerRequired, bool accepted, color c) :
    message(messageId, e_brickAccepted, answerRequired),
    m_accepted(accepted),
    m_color(c)
{

}

bool msg_brickAccepted::isAccepted() const
{
    return m_accepted;
}

color msg_brickAccepted::getColor() const
{
    return m_color;
}

msg_storageQuery::msg_storageQuery(unsigned char messageId, bool answerRequired, color c, unsigned char storageNum) :
    message(messageId, e_storageQuery, answerRequired),
    m_color(c),
    m_storageNum(storageNum)
{

}

color msg_storageQuery::getColor() const
{
    return m_color;
}

unsigned char msg_storageQuery::getStorageNum() const
{
    return m_storageNum;
}

msg_brickPushed::msg_brickPushed(unsigned char messageId, bool answerRequired, bool pushed, unsigned char storageNum) :
    message(messageId, e_brickPushed, answerRequired),
    m_pushed(pushed),
    m_storageNum(storageNum)
{

}

bool msg_brickPushed::isPushed() const
{
    return m_pushed;
}

unsigned char msg_brickPushed::getStorageNum() const
{
    return m_storageNum;
}

msg_startConveyor::msg_startConveyor(unsigned char messageId, bool answerRequired) :
    message(messageId, e_startConveyor, answerRequired)
{

}

msg_stopConveyor::msg_stopConveyor(unsigned char messageId, bool answerRequired) :
    message(messageId, e_stopConveyor, answerRequired)
{

}

msg_ready::msg_ready(unsigned char messageId, bool answerRequired) :
    message(messageId, e_ready, answerRequired)
{

}

msg_brickStored::msg_brickStored(unsigned char messageId, bool answerRequired) :
    message(messageId, e_brickStored, answerRequired)
{

}

msg_finalPosition::msg_finalPosition(unsigned char messageId, bool answerRequired) :
    message(messageId, e_finalPosition, answerRequired)
{

}

msg_goInitialPosition::msg_goInitialPosition(unsigned char messageId, bool answerRequired) :
    message(messageId, e_goInitialPosition, answerRequired)
{

}

msg_disconnected::msg_disconnected(unsigned char messageId, bool answerRequired, moduleType modType, char parameter) :
    message(messageId, e_disconnected, answerRequired),
    m_moduleType(modType),
    m_parameter(parameter)
{

}

moduleType msg_disconnected::getModuleType() const
{
    return m_moduleType;
}

char msg_disconnected::getParameter() const
{
    return m_parameter;
}
