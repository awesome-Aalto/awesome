#ifndef SYSTEM_MSG_WRAPPER_H
#define SYSTEM_MSG_WRAPPER_H

#include "color.h"
#include "message.h"


/**
 * Implements wrapper function for each system message
 */
class systemMsgWrapper
{
public:

    //<summary>Sends message ok</summary>
    void ok();

    //<summary>Sends message connected</summary>
    //<param name="type">Type of the sending module</param>
    void connected(moduleType type, unsigned char param = 0);

    //<summary>Sends message init</summary>
    void init();

    //<summary>Sends message stop</summary>
    void stop();

    //<summary>Sends message wait</summary>
    void wait();

    //<summary>Sends message error</summary>
    void error();

    //<summary>Sends message colorQuery</summary>
    //<param name="c">Color of the brick</param>
    void colorQuery(color c);

    //<summary>Sends message queryOk</summary>
    //<param name="isOk">Is the previously queried thing ok</param>
    void queryOk(bool isOk);

    //<summary>Sends message brickAccepted</summary>
    //<param name="accepted">Was the brick accepted</param>
    //<param name="c">Color of the brick</param>
    void brickAccepted(bool accepted, color c);

    //<summary>Sends message storageQuery</summary>
    //<param name="c">Color of the brick</param>
    //<param name="storageNum">Number of the storage</param>
    void storageQuery(color c, unsigned char storageNum);

    //<summary>Sends message brickPushed</summary>
    //<param name="pushed">Was the brick pushed</param>
    //<param name="c">Color of the brick</param>
    void brickPushed(bool pushed, unsigned char sn);

    //<summary>Sends message startConveyor</summary>
    void startConveyor();

    //<summary>Sends message stopConveyor</summary>
    void stopConveyor();

    //<summary>Sends message ready</summary>
    void ready();

    //<summary>Sends message brickStored</summary>
    void brickStored();

    //<summary>Sends message finalPosition</summary>
    void finalPosition();

    //<summary>Sends message goInitialPosition</summary>
    void goInitialPosition();

protected:

    message* parseMessage(const unsigned char* buf, int len);

    /**
    <summary>Sends the specified message to the host</summary>
    <param name="type">Type of the message</param>
    <param name="answerRequired">Specifies whether this message needs an answer</param>
    <param name="param1">First parameter</param>
    <param name="param2">Second parameter</param>
    **/
    virtual void sendMessage(messageType type, bool answerRequired, unsigned char param1, unsigned char param2) = 0;

};


#endif
