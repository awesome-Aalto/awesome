#include "systemMsgWrapper.h"



void systemMsgWrapper::ok()
{
    sendMessage(e_ok, false, 0, 0);
}

void systemMsgWrapper::connected(moduleType type, unsigned char param)
{
    sendMessage(e_connected, true, (unsigned char)type, param);
}

void systemMsgWrapper::init()
{
    sendMessage(e_init, true, 0, 0);
}

void systemMsgWrapper::stop()
{
    sendMessage(e_stop, true, 0, 0);
}

void systemMsgWrapper::wait()
{
    sendMessage(e_wait, false, 0, 0);
}

void systemMsgWrapper::error()
{
    sendMessage(e_error, true, 0, 0);
}

void systemMsgWrapper::colorQuery(color c)
{
    sendMessage(e_colorQuery, true, (unsigned char)c, 0);
}

void systemMsgWrapper::queryOk(bool isOk)
{
    sendMessage(e_queryOk, true, (isOk ? 1 : 0), 0);
}

void systemMsgWrapper::brickAccepted(bool accepted, color c)
{
    sendMessage(e_brickAccepted, true, (accepted ? 1 : 0), (unsigned char)c);
}

void systemMsgWrapper::storageQuery(color c, unsigned char storageNum)
{
    sendMessage(e_storageQuery, true, (unsigned char)c, storageNum);
}

void systemMsgWrapper::brickPushed(bool pushed, unsigned char sn)
{
    sendMessage(e_brickPushed, true, (pushed ? 1 : 0), sn);
}

void systemMsgWrapper::startConveyor()
{
    sendMessage(e_startConveyor, true, 0, 0);
}

void systemMsgWrapper::stopConveyor()
{
    sendMessage(e_stopConveyor, true, 0, 0);
}

void systemMsgWrapper::ready()
{
    sendMessage(e_ready, true, 0, 0);
}

void systemMsgWrapper::brickStored()
{
    sendMessage(e_brickStored, true, 0, 0);
}

void systemMsgWrapper::finalPosition()
{
    sendMessage(e_finalPosition, true, 0, 0);
}

void systemMsgWrapper::goInitialPosition()
{
    sendMessage(e_goInitialPosition, true, 0, 0);
}


message* systemMsgWrapper::parseMessage(const unsigned char* msg_ptr, int len)
{
    (void)len; // should be used to check the msg array is long enough
    unsigned char messageId = msg_ptr[0];
    messageType type = (messageType)msg_ptr[1];
    bool answerRequired = (bool)(msg_ptr[2]);

    if (type == e_ok)
    {
        return new msg_ok(messageId, answerRequired);
    }
    else if (type == e_connected)
    {
        moduleType modType = (moduleType)msg_ptr[3];
        char parameter = (char)msg_ptr[4];
        return new msg_connected(messageId, answerRequired, modType, parameter);
    }
    else if (type == e_init)
    {
        return new msg_init(messageId, answerRequired);
    }
    else if (type == e_stop)
    {
        return new msg_stop(messageId, answerRequired);
    }
    else if (type == e_wait)
    {
        return new msg_wait(messageId, answerRequired);
    }
    else if (type == e_error)
    {
        return new msg_error(messageId, answerRequired);
    }
    else if (type == e_colorQuery)
    {
        color c = (color)msg_ptr[3];
        return new msg_colorQuery(messageId, answerRequired, c);
    }
    else if (type == e_queryOk)
    {
        bool isOk = (bool)(msg_ptr[3]);
        return new msg_queryOk(messageId, answerRequired, isOk);
    }
    else if (type == e_brickAccepted)
    {
        bool accepted = (bool)(msg_ptr[3]);
        color c = (color)msg_ptr[4];
        return new msg_brickAccepted(messageId, answerRequired, accepted, c);
    }
    else if (type == e_storageQuery)
    {
        color c = (color)msg_ptr[3];
        unsigned char storageNum = msg_ptr[4];
        return new msg_storageQuery(messageId, answerRequired, c, storageNum);
    }
    else if (type == e_brickPushed)
    {
        bool pushed = (bool)(msg_ptr[3]);
        color c = (color)msg_ptr[4];
        return new msg_brickPushed(messageId, answerRequired, pushed, c);
    }
    else if (type == e_startConveyor)
    {
        return new msg_startConveyor(messageId, answerRequired);
    }
    else if (type == e_stopConveyor)
    {
        return new msg_stopConveyor(messageId, answerRequired);
    }
    else if (type == e_ready)
    {
        return new msg_ready(messageId, answerRequired);
    }
    else if (type == e_brickStored)
    {
        return new msg_brickStored(messageId, answerRequired);
    }
    else if (type == e_finalPosition)
    {
        return new msg_finalPosition(messageId, answerRequired);
    }
    else if (type == e_goInitialPosition)
    {
        return new msg_goInitialPosition(messageId, answerRequired);
    }
    else if (type == e_disconnected)
    {
        moduleType modType = (moduleType)msg_ptr[3];
        char parameter = (char)msg_ptr[4];
        return new msg_disconnected(messageId, answerRequired, modType, parameter);
    }
    return nullptr;
}
