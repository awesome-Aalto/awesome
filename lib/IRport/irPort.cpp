#include <Arduino.h>

#include "irPort.h"


irPort::irPort(unsigned char pin)
    : _pin(pin)
{
    pinMode(_pin, INPUT_PULLUP);
}


void irPort::attachInterruptFn(IntFn f, int mode)
{
    attachInterrupt(_pin, f, mode);
}


bool irPort::getStatus() const
{
    return digitalRead(_pin);
}
