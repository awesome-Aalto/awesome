#ifndef PG26_IRPORT_H
#define PG26_IRPORT_H


class irPort
{
public:
    irPort(unsigned char pin);

    typedef void (*IntFn)(void);

    void attachInterruptFn(IntFn f, int mode);

    bool getStatus() const;

private:
    unsigned char _pin;
};
#endif
