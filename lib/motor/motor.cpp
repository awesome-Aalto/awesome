#include "Arduino.h"

#include "motor.h"

#define MAXSPEED 1023
#define MINSPEED 0

motor::motor(uint8_t motorDirection, uint16_t motorSpeed, String name) :
    m_motorDirection(motorDirection),
    m_motorSpeed(motorSpeed),
    m_name(name),
    m_lastSpeed(0),
    m_direction(LOW),
    m_running(false)
{
    //Set motor pins as outputs and store them
    pinMode(m_motorDirection, OUTPUT);
    pinMode(m_motorSpeed, OUTPUT);
}

void motor::forward()
{
    this->forward(m_lastSpeed);
}

void motor::forward(uint16_t speed)
{
    this->startInDirection(MAXSPEED, LOW);
    delay(100);
    this->setSpeed(speed);
}

void motor::backward()
{
    this->backward(m_lastSpeed);
}

void motor::backward(uint16_t speed)
{
    this->startInDirection(MAXSPEED, HIGH);
    delay(100);
    this->setSpeed(speed);
}

void motor::startInDirection(uint16_t speed, byte dir)
{
    if (!m_running)
    {
        speed = clipSpeed(speed);

        digitalWrite(m_motorDirection, dir);
        analogWrite(m_motorSpeed, speed);

        //Store last speed
        m_lastSpeed = speed;
        m_direction = dir;

        Serial.print("Run with speed: ");
        Serial.println(speed);
        printDirection();
        m_running = true;
    }
}

void motor::setSpeed(uint16_t speed)
{
    speed = clipSpeed(speed);

    Serial.print("Set motor speed to: ");
    Serial.println(speed);
    analogWrite(m_motorSpeed, speed);

    m_lastSpeed = speed;
}

void motor::start()
{
    if (!m_running)
    {
        analogWrite(m_motorSpeed, m_lastSpeed);
        digitalWrite(m_motorDirection, m_direction);

        Serial.println("Start the motor in the last direction with last speed.");
        Serial.print("Speed: ");
        Serial.println(m_lastSpeed);
        printDirection();
        m_running = true;
    }
}

void motor::stop()
{
    if (m_running)
    {
        analogWrite(m_motorSpeed, 0);
        digitalWrite(m_motorDirection, LOW);
        Serial.println("Stop the motor");
        m_running = false;
    }
}

void motor::printData()
{
    Serial.print("Motor name: ");
    Serial.println(m_name);
    Serial.print("  -Motor pins are: ");
    Serial.print(m_motorDirection, DEC);
    Serial.print(" (direction), ");
    Serial.print(m_motorSpeed, DEC);
    Serial.println(" (speed)");
    Serial.print("-Last Speed: ");
    Serial.println(m_lastSpeed, DEC);
    printDirection();
}

uint16_t motor::clipSpeed(uint16_t speed)
{
    //Sanity check speed value
    if (speed < MINSPEED)
    {
        speed = MINSPEED;
    }
    else if (speed > MAXSPEED)
    {
        speed = MAXSPEED;
    }
    return speed;
}

void motor::printDirection()
{
    if (m_direction)
    {
        Serial.println("Direction: backward");
    }
    else
    {
        Serial.println("Direction: forward");
    }
}
