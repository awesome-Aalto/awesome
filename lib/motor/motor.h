#ifndef PG26_MOTOR_H
#define PG26_MOTOR_H

///Motor shield class to run DC-motors.
class motor
{
public:
    ///<summary>Motor class constructor</summary>
    ///<param name="motorDirection">Pin that controls the direction of the motor</param>
    ///<param name="motorSpeed">Pin that controls speed of the motor (0-1023)</param>
    ///<param name="name">Name of the motor</param>
    motor(uint8_t motorDirection, uint16_t motorSpeed, String name);

    ///<summary>Set motor run forward with last speed</summary>
    void forward();

    ///<summary>Set motor run forward with wanted speed (0-1023)</summary>
    ///<param name="speed">Speed for the motor. Value can be between 0 to 1023</param>
    void forward(uint16_t speed);

    ///<summary>Set motor run backward with last speed</summary>
	  void backward();

    ///<summary>Set motor run backward with wanted speed (0-1023)</summary>
    ///<param name="speed">Speed for the motor. Value can be between 0 to 1023</param>
	  void backward(uint16_t speed);

    ///<summary>Set speed value for the motor</summary>
    ///<param name="speed">Speed for the motor. Value can be between 0 to 1023</param>
    void setSpeed(uint16_t speed);

    ///<summary>Start the motor in the last direction with the last speed</summary>
    void start();

    ///<summary>Stop the motor</summary>
    void stop();

    ///<summary>Prints motor's data in the serial monitor</summary>
	void printData();

private:
    ///<summary>Starts motor in wanted direction with wanted speed</summary>
    ///<param name="speed">Speed for the motor. Value can be between 0 to 1023</param>
    ///<param name="dir">Direction of the motor (LOW is forward, HIGH is backward). This also depends of the wiring of the motor.</param>
	void startInDirection(uint16_t speed, byte dir);

    bool isRunning();

    uint8_t m_motorDirection;
    uint16_t m_motorSpeed;
    String m_name;
    uint16_t m_lastSpeed;
    byte m_direction;
    bool m_running;
    void printDirection();
    uint16_t clipSpeed(uint16_t s);
};

#endif
