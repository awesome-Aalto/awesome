#ifndef PG26_MULTIPLEXER_H
#define PG26_MULTIPLEXER_H

#include "Arduino.h"
#include "Wire.h"

#define TCAADDR 0x70

class multiplexer
{
public:
    multiplexer();

    void setPort(uint8_t port);

    int getPort();

private:
    int m_activePort;
};

#endif
