#include "parameters.h"
#include "multiplexer.h"

multiplexer::multiplexer() :
    m_activePort(-1)
{}

void multiplexer::setPort(uint8_t port)
{
    if (port > 7) return;

    Wire.beginTransmission(TCAADDR);
    Wire.write(1 << port);
    Wire.endTransmission();
    m_activePort = port;
}

int multiplexer::getPort()
{
    return m_activePort;
}
