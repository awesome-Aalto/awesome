
#include "mqttClientThing.h"

mqttClientThing::mqttClientThing(const char* sendTopic, const char* listenTopic) :
    m_wifi(),
    m_pubSub(MQTT_HOST_IP, MQTT_HOST_PORT, m_wifi),
    m_sentLength(0),
    m_receiveLength(0),
    m_status(false),
    m_newMessage(false)
{
    strcpy(m_sendTopic, sendTopic);
    strcpy(m_listenTopic, listenTopic);
}

void mqttClientThing::connect(moduleType type, unsigned char param)
{
    delay(10);
    m_status = false;
    char will_buffer[MSG_SIZE + 1];
    memset(will_buffer, 0, MSG_SIZE + 1);

    will_buffer[0] = 1;  // id = 0
    will_buffer[1] = 18; // msg_type = e_disconnected
    will_buffer[2] = 1;  // answerRequired = true
    will_buffer[3] = (char)type;
    will_buffer[4] = (char)param;
    will_buffer[5] = '\0';

    Serial.print("\n");
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(WIFI_SSID);

    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

    while (WiFi.status() != WL_CONNECTED)
    {
        delay(250);
        Serial.print(".");
    }

    Serial.print("\n");
    Serial.println("Connected to wifi");

    Serial.print("\n");
    Serial.println("Attempting MQTT connection");

    m_pubSub.setCallback(std::bind(&mqttClientThing::callback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    while (!m_pubSub.connected())
    {
        Serial.print(".");
        m_pubSub.connect(m_sendTopic+5, m_sendTopic, 2, false, will_buffer);
        delay(250);
    }
    m_pubSub.subscribe(m_listenTopic, 0);
    delay(50);
    m_status = status();

    Serial.print("\n");
    Serial.println("MQTT connection on");
}

void mqttClientThing::disconnect()
{
    // TO DO...
}

bool mqttClientThing::status()
{
    if (WiFi.status() == WL_CONNECTED && m_pubSub.connected())
    {
        m_status = true;
    }
    else
    {
        m_status = false;
    }

    return m_status;
}

void mqttClientThing::callback(const char* topic, const uint8_t* payload, unsigned int length)
{
    /**
    Serial.print("\n");
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    **/

    int i = 0;
    for (; i < length && i < MSG_SIZE; i++)
    {
        m_receiveBuffer[i] = payload[i];
    }
    m_receiveLength = i;

    m_newMessage = true;
}


unsigned long mqttClientThing::publish(const unsigned char* buffer, unsigned int length, const char* topic)
{
    length = (length > MSG_SIZE) ? MSG_SIZE : length;
    memcpy(m_sentBuffer, buffer, length);
    m_sentLength = length;
    resendLastMessage();
    return millis();
}


void mqttClientThing::resendLastMessage()
{
    m_pubSub.publish(m_sendTopic, m_sentBuffer, m_sentLength);
}


const unsigned char* mqttClientThing::getMessage(unsigned int* length)
{
    *length = m_receiveLength;
    return m_receiveBuffer;
}


bool mqttClientThing::checkMessageStatus()
{
    m_pubSub.loop();
    if (m_newMessage)
    {
        m_newMessage = false;
        return true;
    }
    else
    {
        return false;
    }
}
