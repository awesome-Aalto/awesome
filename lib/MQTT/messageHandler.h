#ifndef PG26_MESSAGEHANDLER_H
#define PG26_MESSAGEHANDLER_H

#include <functional>

#include "systemMsgWrapper.h"
#include "mqttClientThing.h"
#include "message.h"
#include "parameters.h"


class messageHandler : public systemMsgWrapper
{
public:

    messageHandler(const char* sendTopic, const char* listenTopic);

    //<summary>Connects to a pre-defined WLAN network and starts MTTQ connection to a specified host</summary>
    void connect(moduleType type, unsigned char param);

    //<summary>Ends the MTTQ connection and disconnects from the WLAN</summary>
    void disconnect();

    //<summary>Status of the connection. True if everything is in order</summary>
    bool status();

    /**
     * Returns a new state based on the current state and the contents of possible new messages
     * @return True if all is good and false if resending counter is exceeded.
     */
    bool update();

    //<summary>This sets a function for the update() method to use</summary>
    void setCallback(std::function<void(message*)> callback);

private:

    /**
    <summary>Sends the specified message to the host</summary>
    <param name="type">Type of the message</param>
    <param name="answerRequired">Specifies whether this message needs an answer</param>
    <param name="param1">First parameter</param>
    <param name="param2">Second parameter</param>
    **/
    void sendMessage(messageType type, bool answerRequired, unsigned char param1, unsigned char param2);



    mqttClientThing m_mqtt;
    std::function<void(message*)> callback;
    uint8_t m_nextMessageId;
    uint8_t m_lastReceivedId;
    int m_lastSentTime;
    bool m_lastRequireAnswer;
    uint8_t m_resendCounter;

};

#endif
