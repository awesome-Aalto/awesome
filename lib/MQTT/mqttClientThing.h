#ifndef PG26_MQTTCLIENT_THING_H
#define PG26_MQTTCLIENT_THING_H

#include "PubSubClient.h"
#include "ESP8266WiFi.h"

#include "parameters.h"



///MQTT class to handle the wireless communication.
class mqttClientThing
{
public:
    /**
    <summary>MTTQ Client class constructor</summary>
    <param name="name">Name of this module</param>
    <param name="sendTopic">The topic, or message group, that this module should send its messages to</param>
    <param name="listenTopic">The topic, or message group, that this module should listen</param>
    **/
    mqttClientThing(const char* sendTopic, const char* listenTopic);

    //<summary>Connects to a pre-defined WLAN network and starts MTTQ connection to a specified host</summary>
    void connect(moduleType type, unsigned char param);

    //<summary>Ends the MTTQ connection and disconnects from the WLAN</summary>
    void disconnect();

    //<summary>Status of the connection. True if everything is in order</summary>
    bool status();


    unsigned long publish(const unsigned char* buffer, unsigned int length, const char* topic = NULL);

    //<summary>For use by the PubSubClient library. Please do not ever call this without REALLY knowing what you're doing...</summary>
    void callback(const char* topic, const uint8_t* payload, unsigned int length);

    //<summary>Re-sends previous message again to the host</summary>
    void resendLastMessage();


    const unsigned char* getMessage(unsigned int* length);


    //<summary>Maintains connection and checks if there are new messages</summary>
    bool checkMessageStatus();

private:
    WiFiClient  m_wifi;
    PubSubClient m_pubSub;
    char m_sendTopic[MODULE_TOPIC_MAX_SIZE];
    char m_listenTopic[MODULE_TOPIC_MAX_SIZE];
    unsigned char m_sentBuffer[MSG_SIZE];
    unsigned char m_receiveBuffer[MSG_SIZE];
    unsigned int m_sentLength;
    unsigned int m_receiveLength;
    bool m_status;
    bool m_newMessage;
};

#endif
