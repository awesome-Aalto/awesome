#include <cstring>

#include "messageHandler.h"


messageHandler::messageHandler(const char* sendTopic, const char* listenTopic) :
    m_mqtt(sendTopic, listenTopic),
    m_nextMessageId(1),
    m_lastReceivedId(255),
    m_lastSentTime(0),
    m_lastRequireAnswer(false)
{
    setCallback(NULL);
}

void messageHandler::connect(moduleType type, unsigned char param)
{
    m_mqtt.connect(type, param);
}

void messageHandler::disconnect()
{
    m_mqtt.disconnect();
}

bool messageHandler::status()
{
    return m_mqtt.status();
}


bool messageHandler::update()
{
    bool returnState = true;
    bool unreadMessage = m_mqtt.checkMessageStatus();
    int currentTime = millis();
    bool resendTrigger = (m_lastRequireAnswer && currentTime >= (m_lastSentTime + MSG_RESEND_DELAY));

    // handle unread message
    if (unreadMessage)
    {
        unsigned int length = 0;
        const unsigned char* ptr = m_mqtt.getMessage(&length);
        message* newMessage = parseMessage(ptr, length);
        uint8_t newId = newMessage->getId();

        if (newId != m_lastReceivedId)
        {
            if (newMessage->isAnswerRequired())
            {
                m_nextMessageId = newId;
                m_lastReceivedId = newId;
            }

            callback(newMessage);

            m_resendCounter = 0;
            m_lastRequireAnswer = false;
        }

        delete newMessage;
    }

    // error
    if(m_resendCounter >= MSG_MAX_RESENDS)
    {
        //global error state
        returnState = false;
    }

    // no answer -> resend
    if (!unreadMessage && resendTrigger && m_resendCounter < MSG_MAX_RESENDS)
    {
        m_mqtt.resendLastMessage();
        m_lastSentTime = currentTime;
        m_resendCounter++;
    }

    return returnState;
}

void messageHandler::setCallback(std::function<void(message*)> callback)
{
    this->callback = callback;
}


void messageHandler::sendMessage(messageType type, bool answerRequired, unsigned char param1, unsigned char param2)
{
    unsigned char msg_buffer[MSG_SIZE];
    memset(msg_buffer, 0, MSG_SIZE);

    msg_buffer[0] = m_nextMessageId;
    msg_buffer[1] = (unsigned char)type;
    msg_buffer[2] = (answerRequired ? 1 : 0);
    msg_buffer[3] = param1;
    msg_buffer[4] = param2;

    m_nextMessageId++;

    m_lastSentTime = m_mqtt.publish(msg_buffer, 5);
    m_lastRequireAnswer = answerRequired;
}
