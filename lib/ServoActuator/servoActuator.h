#ifndef PG26_SERVOACTUATOR_H
#define PG26_SERVOACTUATOR_H

//#include "Arduino.h"
#include "Servo.h"

class servoActuator
{
public:
    servoActuator(int pin);

    void extend();

    void retract();

	void push();

	void toReadyPosition();


private:
    int m_pos;
    int m_pin;
    int m_zeroPoint = 0;
    int m_halfPoint = 47;
    int m_fullPoint = 95;
    Servo m_servo;
};

#endif
