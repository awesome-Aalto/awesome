#include "servoActuator.h"

servoActuator::servoActuator(int pin) :
    m_pin(pin),
    m_pos(m_zeroPoint)
{
    pinMode(pin, OUTPUT);
    m_servo.attach(m_pin);
    m_servo.write(m_pos);
}

void servoActuator::extend()
{
    for (; m_pos < m_fullPoint; m_pos += 1)
    {
      m_servo.write(m_pos);
      delay(10);
    }
    if (m_pos >= m_fullPoint)
    {
        m_servo.write(m_fullPoint);
        delay(10);
    }
}

void servoActuator::retract()
{
    for (; m_pos > m_zeroPoint; m_pos -= 1)
    {
      m_servo.write(m_pos);
      delay(10);
    }
    if (m_pos <= m_zeroPoint)
    {
        m_servo.write(m_zeroPoint);
        delay(10);
    }
}

void servoActuator::push()
{
    extend();
    delay(1000);
    retract();
}

void servoActuator::toReadyPosition()
{
    for (; m_pos < m_halfPoint; m_pos += 1)
    {
      m_servo.write(m_pos);
      delay(10);
    }
    if (m_pos >= m_halfPoint)
    {
        m_servo.write(m_halfPoint);
        delay(10);
    }
}
