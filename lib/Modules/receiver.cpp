#include "receiver.h"

//Constructor
receiver::receiver(const String& name) :
    module(receiverPins::e_irPort, name),
    m_receiverMotorSpeedForward(receiverValues::e_receiverMotorSpeedForward),
    m_receiverMotorSpeedBackward(receiverValues::e_receiverMotorSpeedBackward),
    m_colour(unknown),
    m_receiverMotor(receiverPins::e_motorDirPin, receiverPins::e_motorSpeedPin, "Receiver motor"),
    m_greenLED(receiverPins::e_greenLED, "Green led"),
    m_yellowLED(receiverPins::e_yellowLED, "Yellow led"),
    m_redLED(receiverPins::e_redLED, "Red led"),
    m_state(receiver_waitingMsg)
{}

//Thing setup function
void receiver::setup()
{
    // Initialize serial communications
    Serial.begin(9600);

    //run the parent class setup
    module::setup();

    // connect to WiFi
    m_messageHandler.connect(moduleType::receiverType, 1);

    //start I2C communication which RGB sensor then uses
    Wire.begin();

    //send connected message to master
    m_messageHandler.connected(moduleType::receiverType, 0);

    //bind receiver class messagehandler method for general messagehandler for use
    m_messageHandler.setCallback(std::bind(&receiver::handleMessage, this, std::placeholders::_1));
}

//Thing loop function
void receiver::loop()
{
    switch (m_state)
    {
        case receiver_initialize:
            initialize();
        break;
        case receiver_toRunning:
            toRunning();
        case receiver_running:
            running();
        break;
        case receiver_blockDetected:
            blockDetected();
        break;
        case receiver_rejectBlock:
            rejectBlock();
        break;
        case receiver_acceptBlock:
            acceptBlock();
        break;
        case receiver_toWait:
            toWait();
        break;
        case receiver_waiting:
            waiting();
        break;
        case receiver_toStop:
            toStop();
        break;
        case receiver_stopped:
            stopped();
        break;
        case receiver_toError:
            toError();
        break;
        case receiver_error:
            error();
        break;
        case receiver_waitingReplyBrickAccepted:
        case receiver_waitingMsg:
        default:;
    }

    if (!m_messageHandler.update())
    {
        //if receiver state is not already error, move toError state
        if (!(m_state == receiver_error))
        {
            m_state = receiver_toError;
        }
    }
    yield();
}

void receiver::handleMessage(const message* msg)
{
    uint8_t messageId = msg->getId();
    messageType type = msg->getType();
    bool answerRequired = msg->isAnswerRequired();

    Serial.println("================================================");
    Serial.println("Message stuff:");
    Serial.print("message ID: ");
    Serial.println(messageId);
    utilities::printMsgType((int)type);
    if (answerRequired)
        Serial.println("answerRequired: TRUE");
    else
        Serial.println("answerRequired: FALSE");

    if (type == e_ok)
    {
        if (m_state == receiver_waitingReplyBrickAccepted)
        {
            m_state = receiver_toRunning;
            Serial.println("OK message from master to brickaccepted message");
        }
        else
            Serial.println("OK message from master");
    }
    else if (type == e_init)
    {
        m_state = receiver_initialize;
        Serial.println("initializing Receiver");
    }
    else if (type == e_startConveyor)
    {
        m_state = receiver_toRunning;

        //Send ok message to master
        m_messageHandler.ok();

        Serial.println("starting receiver conveyor");
    }
    else if (type == e_stopConveyor || type == e_stop)
    {
        m_state = receiver_toStop;
        Serial.println("stopping receiver conveyor");
    }
    else if (type == e_wait)
    {
        m_state = receiver_toWait;
        Serial.println("Receiver is waiting");
    }
    else if (type == e_queryOk)
    {
        if (msg->isOk())
        {
            m_state = receiver_acceptBlock;
            Serial.println("block accepted");
        }
        else
        {
            m_state = receiver_rejectBlock;
            Serial.println("block rejected");
        }
    }
    else if (type == e_error)
    {
        m_state = receiver_toError;
        Serial.println("receiver is in error");
    }
    else
    {
        Serial.println("Received message, no action taken.");
    }
    Serial.println("================================================");
    Serial.println("");
}

void receiver::initialize()
{
    //Send ok message to master
    m_messageHandler.ok();

    m_receiverMotor.stop();

    //turn all leds on
    ledsOn();

    //Initialize color sensor
    if (!m_sensor.init())
    {
        Serial.println("RGB sensor initialization failed! Tryin initialization again...");
        if (!m_sensor.init())
        {
            Serial.println("RGB sensor initialization failed again! Go to error state.");
            m_state = receiver_toError;
            return;
        }
    }
    delay(500);

    //turn all leds off
    ledsOff();

    //send ready message to master when initialization is done
    m_messageHandler.ready();

    m_state = receiver_waitingMsg;
}

void receiver::toRunning()
{
    m_receiverMotor.stop();

    //turn all leds off
    ledsOff();

    Serial.println("Calibrating");
    //calibrate color sensor
    m_sensor.calibrate(1000, 100);

    //Lit all leds for a moment
    m_greenLED.blink(500);
    m_yellowLED.blink(500);
    m_redLED.blink(500);

    delay(100);

    //turn leds to running state
    m_redLED.off();
    m_yellowLED.off();
    m_greenLED.on();

    //start conveyor
    m_receiverMotor.forward(m_receiverMotorSpeedForward);

    m_state = receiver_running;

    //reset IR flag
    m_irFlag = false;
}

void receiver::running()
{
    //block is detected by the IR port
    if(m_irFlag)
    {
        delay(1);
        if (m_irPort.getStatus())
        {
            m_state = receiver_blockDetected;
        }
    }
}

void receiver::blockDetected()
{
    Serial.println("block detected");
    delay(50);
    m_receiverMotor.stop();
    ledsOff();

    m_yellowLED.on();

    //Sense color
    int senseTime = 1500;
    int measurements = 100;
    int delayTime = 350;

    color c = m_sensor.getColor(senseTime, measurements);

    //error handling
    if(c == unknown)
    {
        c = m_sensor.getColor(senseTime, measurements);
        //second step
        if(c == unknown)
        {
            m_receiverMotor.backward(m_receiverMotorSpeedForward);
            delay(delayTime);
            m_receiverMotor.stop();
            m_irFlag = false;
            m_sensor.calibrate(senseTime, measurements);
            m_receiverMotor.forward(m_receiverMotorSpeedForward);
            while(!m_irFlag)
            {
                yield();
            }
            if(m_irFlag)
            {
                delay(1);
                if (m_irPort.getStatus())
                {
                    delay(100);
                    m_receiverMotor.stop();
                    c = m_sensor.getColor(senseTime, measurements);
                }
            }
        }
    }
    m_colour = c;


    //send color to the master controller
    m_messageHandler.colorQuery(m_colour);

    //move to wait answer from the master controller
    m_state = receiver_waitingMsg;
}

void receiver::rejectBlock()
{
    //do reject block routine and wait reply from the master controller
    ledsOff();

    m_redLED.on();
    m_receiverMotor.backward(m_receiverMotorSpeedBackward);
    delay(2500);

    m_redLED.off();
    m_receiverMotor.stop();

    Serial.println("sending brick accepted query");

    m_messageHandler.brickAccepted(false, m_colour);
    m_state = receiver_waitingReplyBrickAccepted;
    Serial.println("block rejected");
}

void receiver::acceptBlock()
{
    //wait for a while so that sorter has time to start (remove this if it is not needed)
    m_redLED.on();
    m_greenLED.on();
    delay(1500);

    //do accept block routine and wait reply from the master controller
    ledsOff();

    m_receiverMotor.forward(m_receiverMotorSpeedForward);
    m_redLED.on();
    m_greenLED.blink(100);
    delay(100);
    m_greenLED.blink(100);
    delay(100);
    m_greenLED.blink(100);
    delay(100);
    m_greenLED.blink(100);
    delay(100);
    m_greenLED.blink(100);
    delay(100);
    m_redLED.off();
    m_receiverMotor.stop();

    Serial.println("sending brick accepted query");

    m_messageHandler.brickAccepted(true, m_colour);
    m_state = receiver_waitingReplyBrickAccepted;
    Serial.println("block accepted");
}

void receiver::toWait()
{
    m_receiverMotor.stop();
    ledsOff();

    m_messageHandler.ok();
    m_state = receiver_waiting;
}

void receiver::waiting()
{
    delay(500);
    m_yellowLED.blink(500);
}

void receiver::toStop()
{
    m_receiverMotor.stop();
    ledsOff();

    m_redLED.on();
    m_messageHandler.ok();
    m_state = receiver_stopped;
}

void receiver::stopped()
{
    //do nothing
}

void receiver::toError()
{
    m_receiverMotor.stop();
    ledsOff();

    m_messageHandler.error();
    m_state = receiver_error;
    Serial.println("Error!!");
}

void receiver::error()
{
    delay(500);
    m_redLED.blink(500);
}

void receiver::ledsOn()
{
    //turn all leds on
    m_greenLED.on();
    m_yellowLED.on();
    m_redLED.on();
}

void receiver::ledsOff()
{
    //turn all leds off
    m_greenLED.off();
    m_yellowLED.off();
    m_redLED.off();
}
