#ifndef PG26_SORTER_H
#define PG26_SORTER_H

#include "module.h"
#include "message.h"
#include "multiplexer.h"
#include "messageHandler.h"

/**
 * States for the sorter state machine.
 */
enum sorterState
{
    sorter_error = -1,
    sorter_connecting = 10,
    sorter_idling = 20,
    sorter_initialization = 30,
    sorter_starting = 40,
    sorter_running = 50,
    sorter_colorDetection = 60,
    sorter_push = 70,
    sorter_goOn = 80,
    sorter_waiting = 90,
    sorter_stopping = 100
};

/**
 * Struct which represents one of the sorters three submodules.
 */
struct storageSlot
{
    storageSlot(uint8_t servoPin, uint8_t irPin, uint8_t multiplexerPort)
        : m_pusher(servoPin), m_irPort(irPin), m_multiplexerPort(multiplexerPort)
    {}

    colorSensor m_colorSensor;
    servoActuator m_pusher;
    irPort m_irPort;
    uint8_t m_multiplexerPort;
};

/**
 * Helper function for printing the state of the sorter.
 */
void printState(sorterState state);

/**
 * Sorter module class, inherits module abstract class.
 */
class sorter : public module
{
public:
    /**
     * Sorter class basic constructor.
     */
    sorter(const String& name);

    /**
     * The main loop function of the sorter module.
     */
    void loop();

    /**
     * Setup function for sorter class.
     */
    void setup();

private:

    /**
     * Function for sorter_connectiong state.
     */
    void connecting();

    /**
     * Function for sorter_idling state.
     */
    void idling();

    /**
     * Function for sorter_initilization state.
     */
    void initialization();

    /**
     * Function for sorter_starting state.
     */
    void starting();

    /**
     * Function for sorter_running state.
     */
    void running();

    /**
     * Function for sorter_colorDetection state.
     */
    void colorDetection();

    /**
     * Function for sorter_push state.
     */
    void push();

    /**
     * Function for sorter_goOn state.
     */
    void goOn();

    /**
     * Function for sorter_waiting state.
     */
    void waiting();

    /**
     * Function for sorter_stopping state.
     */
    void stopping();

    /**
     * Function for sorter_error state.
     */
    void error();

    /**
     * Senses the color with a color sensor.
     * @param  sensorNum The number of the color sensor to be used [1...3]
     * @return           The sensed color
     */
    color senseColor(uint8_t sensorNum);

    /**
     * Moves a pusher to the position where it will block a brick from passing the pusher.
     * @param pusherNum The number of the pusher [1...3]
     */
    void pusherToReadyPosition(uint8 pusherNum);

    /**
     * Takes care of the logic for pushing a brick to a storage module.
     * @param pusherNum The number of the pusher [1...3]
     */
    void pushBrick(uint8_t pusherNum);

    /**
     * Initializes the storageSlots of the sorter.
     */
    void initStorageSlots();

    /**
     * Calibrates a color sensor.
     * @param sensorNum The number of the sensor to be initialized [1...3]
     */
    void calibrateColorSensor(uint8_t sensorNum);

    /**
     * Interprets the Message msg from the master and updates the state of the sorter accordingly.
     * @param msg The received message
     */
    void handleMessage(const message* msg);

    static void interruptHandler1();
    static void interruptHandler2();
    static void interruptHandler3();

    /**
     * The number of the currently active storageSlot. 0 means that there is no active storageSlot. [0...3]
     */
    uint8_t m_activeStorage;

    /**
     * An array containing the storageSlots.
     */
    storageSlot m_storageSlots[storageSlotCnt];

    /**
     * The color sensed by the sensor.
     */
    color m_sensedColor;

    /**
     * Conveyor motor.
     */
    motor m_motor;

    /**
     * The speed of the conveyor.
     */
    int m_speed;

    /**
     * The current state of the sorter state machine.
     */
    sorterState m_state;

    /**
     * The I2C multiplexer for color sensors
     */
    multiplexer m_multiplexer;

    /**
     * The number of the interrupting irPort. 0 means that there is no active irPort interrupt. [0...3]
     */
    static volatile uint8_t m_irPortNum;

};

#endif
