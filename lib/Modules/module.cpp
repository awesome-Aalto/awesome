#include "module.h"

//Set the interrupt pin as false.
volatile bool module::m_irFlag = false;

//Constructor.
module::module(int irPort, const String& name) :
    m_irPort(irPort),
    m_messageHandler(String("send/"+name).c_str(), String("listen/"+name).c_str())
{}

//Thing setup function.
void module::setup()
{
    m_irPort.attachInterruptFn(&module::interruptHandler, RISING);
    delay(100);
}

//Interrupt handler
void ICACHE_RAM_ATTR module::interruptHandler()
{
    noInterrupts();
    m_irFlag = true;
    interrupts();
}
