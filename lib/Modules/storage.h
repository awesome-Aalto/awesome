#ifndef PG26_STORAGE_H
#define PG26_STORAGE_H

#include "module.h"
#include "palette.h"


/**
 * Storage states
 */
enum storage_state
{
    idling = 0,
    initializing = 10,
    starting = 20,
    stopping = 30,
    running = 40,
    storing = 50,
    erroring = -1
};


/**
 * Storage class implementation, inherits abstract module class
 */
class storage : public module
{
public:

    /**
     * Constructor
     * @param name   name of the module
     * @param num   number of the storage
     */
    storage(const String& name, char num);

    /**
     * Storage initialization routines
     */
    void setup();

    /**
     * Storage main loop. Logic runs here
     */
    void loop();

    /**
     * Callback function to be given to the message handler.
     * Handles received messages.
     * @param  msg Pointer to the message object
     */
    void handleMessage(const message* msg);

private:

    /**
     * Initializes storage
     */
    void initialize();

    /**
     * Waits for brick and stores it
     */
    void run();

    /**
     * Notifies master that error occurred
     */
    void error();

    /**
     * Notifies master that storage is full
     */
    void full();

    /**
     * Starts the conveyor
     */
    void start();

    /**
     * Stops the conveyor
     */
    void stop();

    /**
     * Palette object
     */
    palette m_palette;

    /**
     * Conveyor motor object
     */
    motor m_motor;

    /**
     * Conveyor motor speed
     */
    int m_motorSpeed;

    /**
     * Current storage state
     */
    storage_state m_state;


    char m_storageNum;
};



#endif
