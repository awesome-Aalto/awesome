#ifndef PG26_RECEIVER_H
#define PG26_RECEIVER_H

#include "module.h"

/**
 * Receiver module's states
 */
enum receiver_state
{
    receiver_toError = -2, //something is gone wrong in the receiver module. Stop motor and turn all the leds off. Go to error state.
    receiver_error = -1, //Red LED is blinking.
    receiver_initialize = 0, //initialize colorsensor. conveyor is not moving. Move to waitingMsg state.
    receiver_toRunning = 1, //calibrate color sensor (blink all LEDs) and start conveyor. Then move to running state.
    receiver_running = 2, //all is good and conveyor is moving (waiting for blocks). Green LED is lit.
    receiver_blockDetected = 3, //IR-port senses a block. Its colors is then sensed using the RGB sensor and color information is send to master controller. Wait for answer from master in waitingMsg state. Yellow LED is lit.
    receiver_rejectBlock = 4, //sensed block is not accepted and it will be removed from the system. Red LED is lit. Move to toRunning state.
    receiver_acceptBlock = 5, //sensed block is acceptable and it will be moved forward into the system. Red and green LEDs are blinked when the conveyor moves the brik out of the sensor range. Move to toRunning state.
    receiver_toWait = 6, //master controller asks receiver to wait. Turn leds off and stop the motor.
    receiver_waiting = 7, //receiver is in waiting state. Blink yellow led.
    receiver_toStop = 8, //master controller asks receiver to stop. Set red LED on, stop conveyor. Send "ok" message to master controller.
    receiver_stopped = 9, //receiver is in stopped state. Do nothing.
    receiver_waitingReplyBrickAccepted = 10, //Receiver waits master controller reply for brick accepted message.
    receiver_waitingMsg = 11, //waitingMsg state only loops through the Loop function and waits for messages. State of the motor and LEDs are set in previous state before waitingMsg state.
};

/**
 * Receiver module class
 */
class receiver : public module
{
public:
    /**
     * Receiver class basic constructor
     * @param {char*} name              Name of the board. This is used in communication.
     */
    receiver(const String& name);

    /**
     * The main loop function of the receiver module
     */
    void loop();

    /**
     * Setup function for receiver class
     */
    void setup();

    /**
     * Takes care of the state logic of the module by using the reseived message.
     * @param  msg Received message.
     */
    void handleMessage(const message* msg);

private:
    /**
     * Conveyor motor speed forward.
     */
    int m_receiverMotorSpeedForward;
    /**
     * Conveyor motor speed backward.
     */
    int m_receiverMotorSpeedBackward;
    /**
     * Conveyor motor.
     */
    motor m_receiverMotor;
    /**
     * Green led in the input "traffic lights".
     */
    led m_greenLED;
    /**
     * Yellow led in the input "traffic lights".
     */
    led m_yellowLED;
    /**
     * Red led in the input "traffic lights".
     */
    led m_redLED;
    /**
     * RGB sensor
     */
    colorSensor m_sensor;
    /**
     * Detected color.
     */
    color m_colour;
    /**
     * State of the receiver module logic.
     */
    receiver_state m_state;

    /**
     * Turn all receiver leds on.
     */
    void ledsOn();

    /**
     * Turn all receiver leds off.
     */
    void ledsOff();

    /**
     * initialize receiver module.
     */
    void initialize();

    /**
     * do all the needed things so that receiver can move to running state.
     */
    void toRunning();

    /**
     * receiver is in the running state waiting for interrupt from IR-port.
     */
    void running();

    /**
     * IR-port detects a block. Check its color and ask from the master controller that is is accepted.
     */
    void blockDetected();

    /**
     * master controller tells that the block is not accepted and it will be removed from the receiver.
     */
    void rejectBlock();

    /**
     * master controller tells that the block is accepted and it will continue into the system.
     */
    void acceptBlock();

    /**
     * master controller asks receiver to wait in its current position.
     */
    void toWait();

    /**
     * receiver is waiting in its current position.
     */
    void waiting();

    /**
     * master asks receiver to stop.
     */
    void toStop();

    /**
     * receiver is in stopped state.
     */
    void stopped();

    /**
     * Turn motor and leds off because error is detected.
     */
    void toError();

    /**
     * error state for receiver.
     */
    void error();
};

#endif
