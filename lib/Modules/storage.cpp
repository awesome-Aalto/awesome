#include <functional>
#include <Arduino.h>
#include "storage.h"


storage::storage(const String& name, char storageNum) :
    module(storagePins::irPort1, name),
    m_motor(storagePins::motor1, storagePins::motor2, name+"motor"),
    m_palette((uint8_t)storagePins::stepper1, (uint8_t)storagePins::stepper2, (uint8_t)storagePins::stepper3, (uint8_t)storagePins::stepper4),
    m_motorSpeed(storageMotorSpeed),
    m_state(idling),
    m_storageNum(storageNum)
{}

void storage::setup()
{
    // This call allows us to use Thing's rx-pin (3) as normal gpio
    Serial.begin(9600, SERIAL_8N1, SERIAL_TX_ONLY);

    // parent class setup routines
    module::setup();

    // pwm frequency setting
    analogWriteFreq(stepperPWMfreq);

    m_messageHandler.connect(storageType, m_storageNum);
    m_messageHandler.setCallback(std::bind(&storage::handleMessage, this, std::placeholders::_1));
    Serial.println("Setup complete");
    m_messageHandler.connected(storageType, m_storageNum);
}

void storage::loop()
{
    //The state machine
    switch(m_state)
    {
        case initializing:
            initialize();
            break;
        case starting:
            start();
            break;
        case stopping:
            stop();
            break;
        case running:
            run();
            break;
        case erroring:
            error();
            break;
        case idling:
        default:;
    }
    m_messageHandler.update();
    yield();
}

void storage::handleMessage(const message* msg)
{
    uint8_t messageId = msg->getId();
    messageType type = msg->getType();
    bool answerRequired = msg->isAnswerRequired();

    if (type == e_stop)
    {
        m_state = stopping;
        Serial.println("stopping");
    }
    else if (type == e_init || type == e_goInitialPosition)
    {
        m_state = initializing;
        Serial.println("initializing");
    }
    else if (type == e_startConveyor)
    {
        m_state = starting;
        Serial.println("starting");
    }
    else if (type == e_stopConveyor)
    {
        m_state = stopping;
        Serial.println("stopping");
    }
    else if (type == e_ok)
    {
        m_state = idling;
        Serial.println("idling");
    }
    else
    {
        Serial.println("Received message, no action taken. Debug:");
        Serial.println(messageId);
        Serial.println(type);
        Serial.println(answerRequired);
    }
}

void storage::initialize()
{
    m_messageHandler.ok();
    Serial.println("Zeroing palette");
    m_motor.stop();
    m_palette.zero();
    m_state = idling;
    m_messageHandler.ready();
}

void storage::run()
{
    if (m_irFlag)
    {
        delay(1);
        if (m_irPort.getStatus())
        {
            m_motor.stop();
            delay(10);
            Serial.println("Block detected");
            delay(10);
            m_motor.forward(storageValues::storageMotorBoost);
            delay(1000);
            m_motor.stop();
            delay(1000);
            if (!m_irPort.getStatus())
            {
                Serial.println("Block passed!");
                m_palette.setBlock();
                m_messageHandler.brickStored();

                if (m_palette.isFull())
                {
                    full();
                }
                else
                {
                    m_palette.nextPos();
                    m_state = idling;
                }
            }
            else
            {
                Serial.println("Something wrong");
                m_state = erroring;
            }

        }
        m_irFlag = false;
    }
}

void storage::error()
{
    Serial.println("Error!");
    m_messageHandler.error();
    delay(10000);
}

void storage::start()
{
    if(!m_palette.isFull())
    {
        m_motor.forward(m_motorSpeed);
        m_messageHandler.ok();
        m_state = running;
    }
    else
    {
        full();
    }
}

void storage::stop()
{
    m_motor.stop();
    m_state = idling;
    m_messageHandler.ok();
}

void storage::full()
{
    m_messageHandler.finalPosition();
    m_state = idling;
}
