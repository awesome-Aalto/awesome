#ifndef PG26_MODULE_H
#define PG26_MODULE_H

#include "Arduino.h"
#include "led.h"
#include "colorSensor.h"
#include "motor.h"
#include "servoActuator.h"
#include "irPort.h"
#include "utilities.h"
#include "mqttClientThing.h"
#include "messageHandler.h"
#include "parameters.h"


/**
 * Abstract base class for all the thing modules.
 */
class module
{
    public:
        /**
         * Constructor.
         */
        module(int irPort, const String& name);
        /**
         * Setup function for receiver class
         */
        virtual void setup();
        /**
         * The main loop function of the receiver module
         */
        virtual void loop() = 0;
        /**
        * Set the irFlag HIGH when interrupt occurs
        */
        static void interruptHandler();
        /**
         * handle message received from messagehandler
         */
        virtual void handleMessage(const message* msg) = 0;
    protected:
        /**
         * Interrupt flag.
         */
        static volatile bool m_irFlag;
        /**
         * Interrupt pin.
         */
        irPort m_irPort;

        /**
        * Messagehandler class instance for which handles the messages between the master controller and the module
        */
        messageHandler m_messageHandler;

};

#endif
