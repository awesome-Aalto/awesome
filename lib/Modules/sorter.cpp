#include "sorter.h"

// initialize irPort num variable
volatile uint8_t sorter::m_irPortNum = 0;


//constructor, initalization
sorter::sorter(const String& name):
    module(irPin1, name),
    m_activeStorage(0),
    m_storageSlots({storageSlot(pusherPin1, irPin1, multiplexerPort1)
                    ,storageSlot(pusherPin2, irPin2, multiplexerPort2)
                    ,storageSlot(pusherPin3, irPin3, multiplexerPort3)
                    }),
    m_motor(motorDirPin, motorSpeedPin, "sorterMotor"),
    m_speed(sorterMotorSpeed),
    m_sensedColor(unknown),
    m_state(sorter_connecting)
{}


void ICACHE_RAM_ATTR sorter::interruptHandler1()
{
    noInterrupts();
    m_irFlag = true;
    m_irPortNum = 1;
    interrupts();
}

void ICACHE_RAM_ATTR sorter::interruptHandler2()
{
    noInterrupts();
    m_irFlag = true;
    m_irPortNum = 2;
    interrupts();
}

void ICACHE_RAM_ATTR sorter::interruptHandler3()
{
    noInterrupts();
    m_irFlag = true;
    m_irPortNum = 3;
    interrupts();
}

//thing setup function
void sorter::setup()
{
    // This call allows us to use Thing's rx-pin (3) as normal gpio
    Serial.begin(9600, SERIAL_8N1, SERIAL_TX_ONLY);

    module::setup();

    // connect to WiFi
    m_messageHandler.connect(sorterType, storageSlotCnt);

    // Initializing interrupt handler for each ir-port. These have to be done manually
    m_storageSlots[0].m_irPort.attachInterruptFn(&sorter::interruptHandler1, RISING);
    m_storageSlots[1].m_irPort.attachInterruptFn(&sorter::interruptHandler2, RISING);
    m_storageSlots[2].m_irPort.attachInterruptFn(&sorter::interruptHandler3, RISING);

    Wire.begin();

    m_messageHandler.setCallback(std::bind(&sorter::handleMessage, this, std::placeholders::_1));
    m_messageHandler.connected(sorterType, storageSlotCnt);
    Serial.println("Waiting for connected ok from master");
}

//thing main loop function
void sorter::loop()
{
    // sorter state machine
    switch(m_state)
    {
        case sorter_connecting:
            connecting();
            break;
        case sorter_idling:
            idling();
            break;
        case sorter_initialization:
            initialization();
    		break;
        case sorter_starting:
            starting();
            break;
    	case sorter_running:
            running();
    	    break;
    	case sorter_colorDetection:
            colorDetection();
    		break;
        case sorter_push:
            push();
            break;
        case sorter_goOn:
            goOn();
            break;
        case sorter_waiting:
            waiting();
            break;
        case sorter_stopping:
            stopping();
            break;
        case sorter_error:
            error();
            break;
    }
    m_messageHandler.update();
    yield();
}

void sorter::connecting()
{
}

void sorter::idling()
{
}

void sorter::initialization()
{
    m_motor.stop();

    // inits and calibrates all the colorsensors
    // and sets pushers to initial position
    initStorageSlots();

    Serial.println("Sorter initialized");
    m_state = sorter_idling;
    m_messageHandler.ready();
}


void sorter::starting()
{
    m_motor.forward(m_speed);
    Serial.println("Sorter starting");

    m_state = sorter_running;
    Serial.println("Sorter running");

    m_irFlag = false;
}

void sorter::running()
{
    if (m_irFlag && m_irPortNum > 0)
    {
        delay(1);
        m_motor.stop();
        m_irFlag = false;
        m_activeStorage = m_irPortNum;
        Serial.println("Brick detected!");
        m_state = sorter_colorDetection;
    }
}

void sorter::colorDetection()
{
    m_sensedColor = senseColor(m_activeStorage);

    Serial.print("Is this the correct storage: ");
    Serial.println(m_activeStorage);
    m_messageHandler.storageQuery(m_sensedColor, m_activeStorage);
    Serial.println("Waiting for master response");
    m_state = sorter_waiting;
}

void sorter::waiting()
{
}

void sorter::push()
{
    Serial.println("This is the correct storage -> Push!");

    pusherToReadyPosition(m_activeStorage);
    //wait while the pusher moves
    delay(1000);
    //start the motor to push the brick to the stopper
    m_motor.forward(m_speed);
    // wait while the block moves all the way to the blocker
    delay(2000);
    m_motor.stop();
    //push the brick to the storage
    pushBrick(m_activeStorage);
    //calibrate the color sensor
    calibrateColorSensor(m_activeStorage);
    //tell the master that the brick was pushed
    m_messageHandler.brickPushed(true, m_activeStorage);

    // reset ir-port flags
    m_irFlag = false;
    m_irPortNum = 0;
    m_activeStorage = 0;

    Serial.println("Waiting for master response");
    m_state = sorter_waiting;
}

void sorter::goOn()
{
    Serial.println("This is not the correct storage -> Go on!");

    //tell the master that the brick was not pushed
    m_messageHandler.brickPushed(false, m_activeStorage);
    m_motor.forward(m_speed);
    // wait while the brick moves away from the IR port
    delay(1000);
    m_state = sorter_running;
    m_irFlag = false;
    m_irPortNum = 0;
    m_activeStorage = 0;
}

void sorter::error()
{
    m_motor.stop();
    Serial.println("Error!");
    m_messageHandler.error();
    delay(5000);
}

void sorter::stopping()
{
    Serial.println("Sorter stopping");
    m_motor.stop();
    m_state = sorter_idling;
    Serial.println("Waiting for start command from master");
}

void sorter::initStorageSlots()
{
    for (int i = 1; i <= storageSlotCnt; i++)
    {
        storageSlot& ss = m_storageSlots[i-1];
        m_multiplexer.setPort(m_storageSlots[i-1].m_multiplexerPort);
        delay(500);
        ss.m_colorSensor.init();
        delay(500);
        ss.m_colorSensor.calibrate(colorSensorMeasTime, colorSensorSampleCnt);

        ss.m_pusher.retract();
    }
}

void sorter::calibrateColorSensor(uint8_t num)
{
    if (num > 0 && num <= storageSlotCnt)
    {
        m_multiplexer.setPort(m_storageSlots[num-1].m_multiplexerPort);
        m_storageSlots[num-1].m_colorSensor.calibrate(colorSensorMeasTime, colorSensorSampleCnt);
    }
}

color sorter::senseColor(uint8_t sensorNum)
{
    Serial.println("Sensing color");
    color sensedColor = unknown;

    if (sensorNum > 0 && sensorNum <= storageSlotCnt)
    {
        m_multiplexer.setPort(m_storageSlots[sensorNum-1].m_multiplexerPort);
        //Sense color
        int senseTime = 1500;
        int measurements = 100;
        int delayTime = 350;

        color c = m_storageSlots[sensorNum-1].m_colorSensor.getColor(senseTime, measurements);

        //error handling
        if(c == unknown)
        {
            c = m_storageSlots[sensorNum-1].m_colorSensor.getColor(senseTime, measurements);
            //second step
            if(c == unknown)
            {
                m_motor.backward(m_speed);
                delay(delayTime);
                m_motor.stop();
                m_irFlag = false;
                m_storageSlots[sensorNum-1].m_colorSensor.calibrate(senseTime, measurements);
                m_motor.forward(m_speed);
                while(!m_irFlag)
                {
                    yield();
                }
                if(m_irFlag)
                {
                    delay(1);
                    if (m_storageSlots[sensorNum-1].m_irPort.getStatus())
                    {
                        delay(100);
                        m_motor.stop();
                        c = m_storageSlots[sensorNum-1].m_colorSensor.getColor(senseTime, measurements);
                    }
                }
            }
        }
        sensedColor = c;
    }

    utilities::printColor(sensedColor);

    return sensedColor;
}

void sorter::pusherToReadyPosition(uint8 pusherNum)
{
    if (pusherNum > 0 && pusherNum <= storageSlotCnt)
    {
        m_storageSlots[pusherNum-1].m_pusher.toReadyPosition();
    }
}

void sorter::pushBrick(uint8_t pusherNum)
{
    Serial.println("Pushing brick");
    if (pusherNum > 0 && pusherNum <= storageSlotCnt)
    {
        m_storageSlots[pusherNum-1].m_pusher.push();
    }
    delay(1000);
}

void sorter::handleMessage(const message* msg)
{
    messageType type = msg->getType();

    switch(type)
    {
        case e_ok:
            if (m_state == sorter_connecting)
            {
                Serial.println("Connected to master");
                m_state = sorter_idling;
                Serial.println("Waiting for init command from master");
            }
            else if (m_state == sorter_waiting)
            {
                m_state = sorter_idling;
            }
            break;
        case e_init:
            m_state = sorter_initialization;
            m_messageHandler.ok();
            break;
        case e_startConveyor:
            m_state = sorter_starting;
            m_messageHandler.ok();
            break;
        case e_stopConveyor:
        case e_stop:
            m_state = sorter_stopping;
            m_messageHandler.ok();
            break;
        case e_wait:
            Serial.println("Waiting for master response");
            m_state = sorter_waiting;
            break;
        case e_error:
            m_state = sorter_error;
            break;
        case e_queryOk:
            if (msg->isOk())
            {
                m_state = sorter_push;
            }
            else
            {
                m_state = sorter_goOn;
            }
            m_messageHandler.ok();
            break;
        default:
            m_state = m_state;
    }
}




void printState(sorterState state)
{
    String stateString = "";
    switch(state){
        case sorter_error:
            stateString = "error";
            break;
        case sorter_connecting:
            stateString = "connecting";
            break;
        case sorter_idling:
            stateString = "idling";
            break;
        case sorter_initialization:
            stateString = "initialization";
            break;
        case sorter_starting:
            stateString = "starting";
            break;
        case sorter_running:
            stateString = "running";
            break;
        case sorter_colorDetection:
            stateString = "colorDetection";
            break;
        case sorter_push:
            stateString = "push";
            break;
        case sorter_goOn:
            stateString = "goOn";
            break;
        case sorter_waiting:
            stateString = "wait";
            break;
        case sorter_stopping:
            stateString = "stopping";
            break;
        default:
            stateString = String(int(state));
    }
    Serial.print("State is: ");
    Serial.println(stateString);
}
