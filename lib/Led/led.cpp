#include "Arduino.h"

#include "led.h"

led::led(byte pin, String name) :
    m_pin(pin),
    m_name(name)
{
    //set wanted pin to be output for the led and store this information
    pinMode(pin, OUTPUT);
}

void led::on()
{
    digitalWrite(m_pin, HIGH);
}

void led::off()
{
    digitalWrite(m_pin, LOW);
}

void led::blink(int milliSeconds)
{
    // Led was already on
    if (digitalRead(m_pin) == HIGH)
    {
        this->off();
        delay(milliSeconds);
        this->on();
    }
    else
    {
        this->on();
        delay(milliSeconds);
        this->off();
    }
}

void led::printData()
{
    Serial.print("Led name: ");
    Serial.println(m_name);
    Serial.print("  -LED pin is: ");
    Serial.println(m_pin, DEC);
}
