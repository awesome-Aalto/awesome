#ifndef PG26_LED_H
#define PG26_LED_H

//Class for LED basic functionality.
class led
{
public:
    ///<summary>Led class constructor</summary>
    ///<param name="pin">Pin in which the led is connected</param>
    ///<param name="name">Name of the led</param>
    led(uint8_t pin, String name);

    ///<summary>Turns the led on. If the onboard LED is used, due to its wiring this function turns it off.</summary>
    void on();

	///<summary>Turns the led off. If the onboard LED is used, due to its wiring this function turns it on.</summary>
	void off();

    ///<summary>Blinks the led for wanted duration (ms). (OFF->ON->OFF or ON->OFF->ON depending the state of the led.)</summary>
    ///<param name="milliSeconds">Duration of the blink</param>
    void blink(int milliSeconds);

    ///<summary>Prints led's data in the serial monitor</summary>
	void printData();

private:
    uint8_t m_pin;
    String m_name;
};

#endif
