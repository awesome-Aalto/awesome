#ifndef PG26_UTILITIES_H
#define PG26_UTILITIES_H

#include "color.h"

/**
 * Common utilities
 */
namespace utilities
{
    /**
     * Prints the color to Serial.
     * @param c color to be printed.
     */
    void printColor(color c);

    /**
     * Prints the message type to Serial
     * @param msg message type to be printend.
     */
    void printMsgType(int msgType);
}
#endif
