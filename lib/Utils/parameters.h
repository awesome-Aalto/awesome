#ifndef PG26_PARAMETERS_H
#define PG26_PARAMETERS_H

#define WIFI_SSID "rasputin"
#define WIFI_PASSWORD "leegotkurkussa"
#define MQTT_HOST_IP "192.168.42.1"
#define MQTT_HOST_PORT 1883
#define MODULE_NAME_MAX_SIZE 16
#define MODULE_TOPIC_MAX_SIZE 16
#define MSG_SIZE 6
#define MSG_MAX_RESENDS 8
#define MSG_RESEND_DELAY 5000 // milliseconds

enum moduleType
{
    receiverType = 1,
    sorterType = 2,
    storageType = 3,
    masterType = 4
};

enum receiverPins
{
    e_irPort = 12,
    e_motorDirPin = 0,
    e_motorSpeedPin = 5,
    e_greenLED = 13,
    e_yellowLED = 16,
    e_redLED = 15,
};

enum receiverValues
{
    e_receiverMotorSpeedForward = 550,
    e_receiverMotorSpeedBackward = 650,
};

enum storagePins
{
    // stepper motor control signals
    stepper1 = 5,
    stepper2 = 0,
    stepper3 = 4,
    stepper4 = 13,

    // IRport
    irPort1 = 12,

    // conveyor motor
    motor1 = 16,  //direction
    motor2 = 15,  //speed

    // limit switches
    limitStart = 14,
    limitStop = 3,
};

enum storageValues
{
    storageSlots = 3,
    stepperStepsPerRevolution = 48, // step amount per one full revolution
    stepperStepIncrement = 14, // steps to move pallette to next position
    stepperPWM = 850, // 0...1023
    stepperSpeed = 30, // rpm
    stepperPWMfreq = 17000, //
    storageMotorSpeed = 900,
    storageMotorBoost = 1023
};

enum sorterPins
{
    motorDirPin = 0,
    motorSpeedPin = 5,

    pusherPin1 = 15,
    pusherPin2 = 16,
    pusherPin3 = 12,

    irPin1 = 3,
    irPin2 = 13,
    irPin3 = 4,

    multiplexerPort1 = 2,
    multiplexerPort2 = 4,
    multiplexerPort3 = 7


};

enum sorterValues
{
    sorterMotorSpeed = 900,
    storageSlotCnt = 3,
    colorSensorMeasTime = 1500,
    colorSensorSampleCnt = 100
};

#endif
