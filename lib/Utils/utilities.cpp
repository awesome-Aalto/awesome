#include "utilities.h"
#include "Arduino.h"
//prints the color to serial out
void utilities::printColor(color c)
{
    //print color
    String colstr = "";
    switch(c){
        case red:
            colstr = "red";
            break;
        case green:
            colstr = "green";
            break;
        case blue:
            colstr = "blue";
            break;
        case yellow:
            colstr = "yellow";
            break;
        case cyan:
            colstr = "cyan";
            break;
        case magenta:
            colstr = "magenta";
            break;
        case black:
            colstr = "black";
            break;
        case white:
            colstr = "white";
            break;
        case unknown:
            colstr = "unknown";
            break;
    }
    Serial.print("Color is: ");
    Serial.println(colstr);
}

void utilities::printMsgType(int msgType)
{
    String msgTypeStr = "";
    if (msgType == 1)
        msgTypeStr = "ok";
    else if (msgType == 2)
        msgTypeStr = "connected";
    else if (msgType == 3)
        msgTypeStr = "initialize";
    else if (msgType == 4)
        msgTypeStr = "stop";
    else if (msgType == 5)
        msgTypeStr = "wait";
    else if (msgType == 6)
        msgTypeStr = "error";
    else if (msgType == 7)
        msgTypeStr = "colorQuery";
    else if (msgType == 8)
        msgTypeStr = "queryOK";
    else if (msgType == 9)
        msgTypeStr = "brickAccepted";
    else if (msgType == 10)
        msgTypeStr = "storagePushed";
    else if (msgType == 11)
        msgTypeStr = "brickPushed";
    else if (msgType == 12)
        msgTypeStr = "startConveyor";
    else if (msgType == 13)
        msgTypeStr = "stopConveyor";
    else if (msgType == 14)
        msgTypeStr = "ready";
    else if (msgType == 15)
        msgTypeStr = "brickStored";
    else if (msgType == 16)
        msgTypeStr = "finalPosition";
    else if (msgType == 17)
        msgTypeStr = "goInitialPosition";
    else if (msgType == 18)
        msgTypeStr = "disconnected";

    Serial.print("Message type is: ");
    Serial.println(msgTypeStr);
}
