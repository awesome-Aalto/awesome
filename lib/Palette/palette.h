#ifndef PG26_PALETTE_H
#define PG26_PALETTE_H

#include <Arduino.h>
#include "Stepper.h"



/**
 * Class for pallette functionality
 */
class palette
{
public:

    /**
    * Constructor, initializes stepper motor
    * @param  pin1 stepper motor control signal
    * @param  pin2 stepper motor control signal
    * @param  pin3 stepper motor control signal
    * @param  pin4 stepper motor control signal
    */
    palette(uint8_t pin1, uint8_t pin2, uint8_t pin3, uint8_t pin4);

    /**
     * Notifies pallette a brick is inserted to the current slot
     */
    void setBlock();

    /**
     * Checks if the pallette is full
     * @return True if no slots available
     */
    bool isFull() const;

    /**
     * Moves the pallette to next position if possible
     * @return True on success
     */
    bool nextPos();

    /**
     * Moves the pallette to the initial position
     */
    void zero();

private:

    /**
     * Stepper motor object
     */
    Stepper m_stepper;

    /**
     * Slot position counter
     */
    uint8_t m_pos;

    /**
     * Fullness flag
     */
    bool m_full;

    /**
     * Start position switch pin number
     */
    uint8_t m_beginSwitch;

    /**
     * End position switch pin number
     */
    uint8_t m_endSwitch;
    
};


#endif
