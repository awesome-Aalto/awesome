#include "parameters.h"
#include "palette.h"


palette::palette(uint8_t pin1, uint8_t pin2, uint8_t pin3, uint8_t pin4)
    : m_stepper(stepperStepsPerRevolution, pin1, pin2, pin3, pin4, storageValues::stepperPWM),
    m_pos(0),
    m_full(false),
    m_beginSwitch(storagePins::limitStart),
    m_endSwitch(storagePins::limitStop)
{
    m_stepper.setSpeed(storageValues::stepperSpeed);
    pinMode(m_beginSwitch, INPUT_PULLUP);
    pinMode(m_endSwitch, INPUT_PULLUP);
}

void palette::setBlock()
{
    if (m_pos >= storageSlots-1 || !digitalRead(m_endSwitch))
        m_full = true;
}

bool palette::isFull() const
{
    return m_full;
}

bool palette::nextPos()
{
    if (m_pos < storageSlots-1)
    {
        int step = 0;
        while(digitalRead(m_endSwitch) && step < stepperStepIncrement)
        {
            m_stepper.step(1);
            step++;
            yield();
        }
        if (step >= (stepperStepIncrement-3))
        {
            m_pos++;
            yield();
        }

        return true;
    }

    return false;
}

void palette::zero()
{
    int maxSteps = 0;
    while (digitalRead(m_beginSwitch) || maxSteps > 100)
    {
        m_stepper.step(-1);
        maxSteps++;
        yield();
    }
    m_pos = 0;
    m_full = false;
}
