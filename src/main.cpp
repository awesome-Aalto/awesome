//#define RECEIVER
#define SORTER
//#define STORAGE "storage1"
//#define STORAGE "storage2"
//#define STORAGE "storage3"
//#define STORAGENUM 2


#if defined(RECEIVER)
    #include "receiver.h"
    receiver thing("receiver");
#elif defined(SORTER)
    #include "sorter.h"
    sorter thing("sorter");
#elif defined(STORAGE)
    #include "storage.h"
    storage thing(STORAGE, STORAGENUM);
#endif

/**
 * Modules setup funtion. This is performed once when the board is powered.
 */
void setup()
{
    thing.setup();
}

/**
 * Modules functionality that is looping when the board is running.
 */
void loop()
{
    thing.loop();
}
